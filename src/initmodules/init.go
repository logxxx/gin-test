package initmodules

import (
	"common/inject"
	"common/logutil"
	"controller"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"plugins"
	"plugins/aixrouter"
	"plugins/mainconfig/mainconfigutil"
	"time"
)

func registerApi(r gin.IRouter) {
	r.POST("/user/get_info", controller.UserGetInfo)
	r.GET("/ping2", controller.PING)
	aixrouter.RegisterRoute(r, "/hehe")
}

func RunHttpServer() {
	//创建服务并注册api
	r := controller.CreateServer()
	registerApi(r)

	addr := "0.0.0.0:8080"
	fmt.Println(fmt.Sprintf("[WebServer %s]starting.", addr))
	httpServer := &http.Server{
		Addr:    addr,
		Handler: r,
		ReadTimeout: 5*time.Second,
		WriteTimeout: 5*time.Second,
	}
	if err := httpServer.ListenAndServe(); err != nil {
		panic(err)
	}
}

func MustInitAll() {
	plugins.MustPreInit()

	//初始化日志
	mainCfg := mainconfigutil.GetConfig()
	logutil.InitFlags()
	logutil.SetStringLogLevel(mainCfg.Log.Level)

	plugins.MustInit()

	inject.EnableRoute(mainCfg.Inject.Route)
	inject.MustInject()
	defer inject.Close()
}
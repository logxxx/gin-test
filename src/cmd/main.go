package main

import "initmodules"

func main() {
	defer func() {
		if err := recover(); err != nil {
			panic(err)
		}
	}()

	initmodules.MustInitAll()
	initmodules.RunHttpServer()
}




package controller

import (
	"common/reqresputil"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

type UserGetInfoReq struct {
	reqresputil.Request
	Uid int64 `json:"uid"`
}

type UserGetInfoResp struct {
	reqresputil.Response
	Name string `json:"name"`
}

func UserGetInfo(c *gin.Context) {
	req := &UserGetInfoReq{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		log.Printf("UserGetInfo err:%v", err)
		reqresputil.DoReturn(c, nil, err)
	}
	log.Printf("UserGetInfo GetMsg:%#v", req)

	resp := &UserGetInfoResp{
		Name: fmt.Sprintf("hello,%v", req.Uid),
	}
	reqresputil.DoReturn(c, resp, nil)
}

func PING(c *gin.Context) {
	c.JSON(200, gin.H{"message": fmt.Sprintf("%s:pongpongpong!", time.Now().Format("2006-01-02 15:04:05"))})
}

package controller

import (
	"github.com/gin-gonic/gin"
	"middleware"
	"middleware/cors"
)

func CreateServer() *gin.Engine {

	r := gin.New()

	//中间件
	r.Use(middleware.SetContext(), middleware.Logger())
	r.Use(cors.CORS())

	return r
}

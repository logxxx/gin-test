package plugins

import (
	"plugins/connection"
	"plugins/mainconfig"
	"plugins/mytcc"
	"plugins/ocr"
	"plugins/statistic"
	"plugins/user"
)

func MustPreInit() {
	//底层服务
	mainconfig.MustInit()
}

func MustInit() {
	//基础服务
	connection.MustInit()

	//上层业务
	user.MustInit()
	//task.MustInit()
	//novice.MustInit()
	statistic.MustInit()
	mytcc.MustInit()
	//saving.MustInit()
	ocr.MustInit()
}

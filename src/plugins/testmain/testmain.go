package testmain

import (
	"initmodules"
	"os"
	"plugins/aixconfig"
	"testing"
)

func TestMain(m *testing.M) {
	defer func() {
		if err := recover(); err != nil {
			panic(err)
		}
	}()
	//执行前:biz_test.go的执行目录,如/gin-test/src/plugins/task/taskbiz
	//执行后:/gin-test
	err := os.Chdir("../../../..")
	if err != nil {
		panic(err)
	}

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	aixconfig.SetConfigFolder(dir)
	initmodules.MustInitAll()
	status := m.Run()
	os.Exit(status)
}
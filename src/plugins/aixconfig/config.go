package aixconfig

import (
	"common/logutil"
	"encoding/json"
	"os"
	"path/filepath"
)

var (
	defaultConfigFolder = "./config"
)

func SetConfigFolder(path string) {
	defaultConfigFolder = path+"/config"
}

func init() {
	var err error
	defaultConfigFolder, err = filepath.Abs(defaultConfigFolder)
	if err != nil {
		panic(err)
	}
	logutil.Infof("init() defaultConfigFolder:%v", defaultConfigFolder)
}

func MustLoadPluginConfig(name string, dest interface{}) {
	fullPath := filepath.Join(defaultConfigFolder, name)
	mustLoadJsonConfig(fullPath, dest)
}

func mustLoadJsonConfig(path string, dest interface{}) {
	logutil.Infof("mustLoadJsonConfig path:%v", path)
	fp, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	dec := json.NewDecoder(fp)
	err = dec.Decode(dest)
	if err != nil {
		panic(err)
	}
}

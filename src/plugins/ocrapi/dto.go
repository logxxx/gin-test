package ocrapi

type GeneralAccurateOCRReq struct {
	ImageBase64 string `json:"ImageBase64"`
	ImageUrl    string `json:"ImageUrl"`
}

type GeneralAccurateOCRRespData struct {
	Response GeneralAccurateOCRResp `json:"Response"`
}

type GeneralAccurateOCRResp struct {
	TextDetections []TextDetection `json:"TextDetections"` //检测到的文本信息
	Angel          float64         `json:"Angel"`          //图片旋转角度（角度制），文本的水平方向为0°；顺时针为正，逆时针为负
	RequestId      string          `json:"RequestId"`      //唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。

}

type TextDetection struct {
	DetectedText string `json:"DetectedText"`
	Confidence   int    `json:"Confidence"`
	ItemPolygon  struct {
		X      int `json:"X"`
		Y      int `json:"Y"`
		Width  int `json:"Width"`
		Height int `json:"Height"`
	} `json:"ItemPolygon"`
}

type ApiOcrRespErr struct {
	Response struct {
		RequestId string `json:"RequestId"`
		Error     ErrMsg `json:"Error"`
	} `json:"Response"`
}

type ErrMsg struct {
	Code    string `json:"Code"`
	Message string `json:"Message"`
}

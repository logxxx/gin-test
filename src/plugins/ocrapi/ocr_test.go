package ocrapi_test

import (
	"aix.zhhainiao.com/aixpublic/utils/cos"
	"aix.zhhainiao.com/app/ydz/scannersvc/plugins/ocrapi"
	"context"
	"io/ioutil"
	"testing"
	"time"
)

var (
	CTX = context.Background()
	//测试帐号
	appId     = "1256605913"
	secretId  = "AKIDMUNzkEHTG1VhbDrdQv5yZv0qOoy24lDS"
	secrecKey = "ScU5PTGlzy4sOdeOvB9cpZ2FAoCIgmcA"
)

func TestClient_GeneralAccurateOCR(t *testing.T) {
	client, err := ocrapi.NewClient(secretId, secrecKey, ocrapi.RegionGuangzhou)
	if err != nil {
		t.Fatal(err)
	}
	if client == nil {
		t.Fatal()
	}

	//1.测试一张正常的图片文件
	file1, err := ioutil.ReadFile("./testdata/test1.jpg")
	if err != nil {
		t.Fatal(err)
	}
	resp1, _, err := client.GeneralOCR(CTX, file1, "", false, time.Now())
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("resp1.TextDetections:")
	for i := range resp1.TextDetections {
		t.Logf("%#v", resp1.TextDetections[i])
	}
	t.Logf("resp1.Angel:%v", resp1.Angel)
	t.Logf("resp1.RequestId:%v", resp1.RequestId)

	//2.测试一张错误的图片文件
	file2, err := ioutil.ReadFile("./testdata/test2.jpg")
	if err != nil {
		t.Fatal(err)
	}
	resp2, apiErr2, err := client.GeneralOCR(CTX, file2, "", false, time.Now())
	if err != nil {
		t.Fatal(err)
	}
	if apiErr2 == nil {
		t.Fatal()
	}
	if resp2 != nil {
		t.Fatal()
	}
	t.Logf("apiErr2:%+v", apiErr2)

	//3.测试用cos上传的url
	cosUrl := getCosImgUrl(t)
	t.Logf("cosUrl:%v", cosUrl)
	resp3, _, err := client.GeneralOCR(CTX, nil, cosUrl, false, time.Now())
	if err != nil {
		t.Fatal(err)
	}
	if resp3 == nil {
		t.Fatal()
	}
	t.Logf("resp3.TextDetections:")
	for i := range resp3.TextDetections {
		t.Logf("%#v", resp3.TextDetections[i])
	}
	t.Logf("resp3.Angel:%v", resp3.Angel)
	t.Logf("resp3.RequestId:%v", resp3.RequestId)
}

func TestNewClient(t *testing.T) {
	t.Logf("%#v", getCosImgUrl(t))
}

func getCosImgUrl(t *testing.T) string {
	var testcossconfig = &cos.Config{
		AppID:     appId,
		SecretID:  secretId,
		SecretKey: secrecKey,
		Bucket:    "scanner-king-1253413501",
		Region:    "ap-guangzhou",
	}
	c, err := cos.NewClient(testcossconfig)
	if err != nil {
		t.Fatal(err)
	}

	// 0.0 直接上传个文件
	err = c.Upload(CTX, "/test/test1.jpg", "./testdata/test1.jpg")
	if err != nil {
		t.Fatal(err)
	}

	// 1.0 生成预签名的下载链接
	url, err := c.MakeDownloadURL(CTX, "/test/test1.jpg", "test1.jpg", 60*time.Minute)
	if err != nil {
		t.Fatal(err)
	}
	return url
}

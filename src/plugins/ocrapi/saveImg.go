package ocrapi

import (
	"bytes"
	"common/ctxutil"
	log "common/logutil"
	"context"
	"fmt"
	"github.com/golang/freetype"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"os"
	"plugins/ocr/ocrdto"
	"plugins/ocr/ocrproto"
	"time"
)

func drawRect(ctx context.Context, m *image.RGBA, contents []ocrproto.TextDetections) error {
	for i := range contents {
		//画方框
		rectInfo := contents[i].ItemPolygon
		xStart := rectInfo.X
		yStart := rectInfo.Y
		xEnd := xStart + rectInfo.Width
		yEnd := yStart + rectInfo.Height
		color := color.RGBA{255, 0, 0, 0}
		ocrdto.Rect(xStart, yStart, xEnd, yEnd, 1)(m, color)
	}
	return nil
}

func drawText(ctx context.Context, m *image.RGBA, contents []ocrproto.TextDetections) error {
	//初始化文字工具
	c := freetype.NewContext()
	wd, _ := os.Getwd()
	log.Infof("drawText wd:%v", wd)
	dirs := make([]string, 0)
	files, _ := ioutil.ReadDir("./")
	for _, f := range files {
		dirs = append(dirs, f.Name(), " ")
	}
	log.Infof("dirs:%v", dirs)

	fontData, err := ioutil.ReadFile("src/plugins/ocrapi/zhongwen.TTF")
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "DoOcr ioutil.ReadFile err:(%v)", err))
		return err
	}
	fontObj, err := freetype.ParseFont(fontData)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "DoOcr freetype.ParseFont err:(%v)", err))
		return err
	}
	c.SetFont(fontObj)
	c.SetFontSize(14)
	c.SetClip(m.Bounds())
	c.SetSrc(image.Black)
	c.SetDst(m)

	for i := range contents {
		rect := contents[i].ItemPolygon
		content := fmt.Sprintf("%v(%v)", contents[i].DetectedText, contents[i].Confidence)
		_, err = c.DrawString(content, freetype.Pt(rect.X, rect.Y+rect.Height))
		if err != nil {
			log.Errorf("saveImg c.DrawString err:%v", err)
			return err
		}
	}
	return nil
}

func GeneImg(ctx context.Context, p *ocrdto.DoGeneralOcrParams, dto *ocrdto.DoGeneralOcrDto) ([]byte, error) {
	var err error
	log.Infof("GeneImg p.ImgFile len:%v", len(p.ImgFile))
	imgData := []byte(p.ImgFile)

	var imgObj image.Image
	switch p.Format {
	case ocrdto.FileFormatJPEG, ocrdto.FileFormatJPG:
		imgObj, err = jpeg.Decode(bytes.NewBuffer(imgData))
	case ocrdto.FileFormatPNG:
		imgObj, err = png.Decode(bytes.NewBuffer(imgData))
	}
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "saveImg Decode err:(%v)", err))
		return nil, err
	}

	bounds := imgObj.Bounds()
	newBounds := image.Rectangle{
		Min: bounds.Min,
		Max: image.Point{
			X: bounds.Max.X * 2, //新画布为２张图片横排的大小
			Y: bounds.Max.Y,
		},
	}
	m := image.NewRGBA(newBounds)

	//上白底
	draw.Draw(m, newBounds, &image.Uniform{color.RGBA{255, 255, 255, 255}}, image.ZP, draw.Src)

	//上原图
	sourceBounds := image.Rectangle{
		Min: image.Point{
			X: bounds.Max.X, //放到右边
			Y: bounds.Min.Y,
		},
		Max: image.Point{
			X: bounds.Max.X * 2,
			Y: bounds.Max.Y,
		},
	}
	draw.Draw(m, sourceBounds, imgObj, image.ZP, draw.Src)

	//画方框
	err = drawRect(ctx, m, dto.OcrResult.TextDetections)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "saveImg drawRect err:(%v)", err))
		return nil, err
	}

	//画文字
	err = drawText(ctx, m, dto.OcrResult.TextDetections)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "saveImg drawText err:(%v)", err))
		return nil, err
	}

	buf := &bytes.Buffer{}
	err = jpeg.Encode(buf, m, &jpeg.Options{Quality: 80})
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "saveImg jpeg.Encode err:(%v)", err))
		return nil, err
	}

	return buf.Bytes(), nil
}

func SaveImgToLocal(ctx context.Context, p *ocrdto.DoGeneralOcrParams, dto *ocrdto.DoGeneralOcrDto) error {
	res, err := GeneImg(ctx, p, dto)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "SaveImgToLocal GeneImg err:(%v)", err))
		return err
	}

	//保存到文件
	outputFile, err := os.Create(fmt.Sprintf("src/plugins/ocr/ocrdto/pic_output/%v.jpeg", time.Now().Format("20060102_150405")))
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "saveImg os.Create err:(%v)", err))
		return err
	}
	defer outputFile.Close()
	outputFile.Write(res)

	return nil
}

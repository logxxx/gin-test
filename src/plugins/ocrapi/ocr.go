package ocrapi

import (
	"bytes"
	"common/code"
	"common/ctxutil"
	log "common/logutil"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var (
	DefaultTimeout   = 5 * time.Second
	DefaultVersion   = "2018-11-19"
	DefaultUri       = "ocr.tencentcloudapi.com"
	DefaultUriPrefix = "https://"
	RegionGuangzhou  = "ap-guangzhou"

	ActionGeneralAccurateOCR = "GeneralAccurateOCR"
	ActionGeneralFastOCR     = "GeneralFastOCR"
)

type Client struct {
	httpClient http.Client
	cfg        Config
}

//创建一个ocr服务
func NewClient(secretId, secretKey string, region string) (*Client, error) {
	return &Client{
		httpClient: http.Client{
			Timeout: DefaultTimeout,
		},
		cfg: Config{ //一些固定不变的参数，在生成对象时初始化
			SecretId:  secretId,
			SecretKey: secretKey,
			Version:   DefaultVersion,
			Region:    region,
			Uri:       DefaultUri,
			UriPrefix: DefaultUriPrefix,
		},
	}, nil
}

//通用文字识别相关接口-通用印刷体识别（高精度版)
//imgFile:文件内容,ioutil.ReadFile()的结果
//imgURL:文件的网络地址。
//图片的 imgFile、imgURL 必须提供一个，如果都提供，只使用 imgURL。(腾讯ｏｃｒ服务的规则)
func (c *Client) GeneralOCR(ctx context.Context, imgFile []byte, imgUrl string, isAccurate bool, now time.Time) (*GeneralAccurateOCRResp, *ApiOcrRespErr, error) {

	c.cfg.Action = ActionGeneralFastOCR //默认快速识别
	if isAccurate {
		c.cfg.Action = ActionGeneralAccurateOCR //开启"高精度识别"
	}

	if len(imgFile) == 0 && len(imgUrl) == 0 {
		return nil, nil, code.BadRequest
	}
	apiReq := &GeneralAccurateOCRReq{
		ImageUrl: imgUrl,
	}
	if apiReq.ImageUrl == "" { //只有当url为空时，才给文件字段赋值
		apiReq.ImageBase64 = base64.StdEncoding.EncodeToString(imgFile)
	}

	//请求ocr接口
	apiResp, err := c.post(ctx, apiReq, now)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "GeneralOCR c.post err:(%v)", err))
		return nil, nil, err
	}

	//api返回的json转为结构体
	objResp := &GeneralAccurateOCRRespData{}
	err = json.Unmarshal(apiResp, objResp)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "GeneralOCR json.Unmarshal err:%v", err))
		return nil, nil, err
	}
	if len(objResp.Response.TextDetections) == 0 { //有可能是错误信息.
		respErrObj := &ApiOcrRespErr{}
		err = json.Unmarshal(apiResp, respErrObj)
		if err != nil {
			log.Errorf(ctxutil.F(ctx, "Post json.Unmarshal err:%v", err))
			return nil, nil, err
		}
		if respErrObj.Response.Error.Code != "" { //确定是有错误信息
			return nil, respErrObj, nil
		}
	}
	return &objResp.Response, nil, nil
}

func (c *Client) post(ctx context.Context, request interface{}, now time.Time) ([]byte, error) {
	bf := bytes.NewBuffer([]byte{})
	jsonEncoder := json.NewEncoder(bf)
	jsonEncoder.SetEscapeHTML(false)
	err := jsonEncoder.Encode(request)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "jsonEncoder.Encode err:(%v)", err))
		return nil, err
	}
	log.Infof("bf.String():%v", bf.String())

	reqData := strings.NewReader(bf.String())
	req, err := http.NewRequest("POST", c.cfg.getPath(), reqData)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "Post NewRequest err:(%v)", err))
		return nil, err
	}
	err = c.setHeader(req, bf.Bytes(), now)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "Post setHeader err:(%v)", err))
		return nil, err
	}

	log.Infof("post req:%+v", req)
	resp, err := c.httpClient.Do(req)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "Post biz.httpClient.Do err:%v", err))
		return nil, err
	}
	defer resp.Body.Close()
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Errorf(ctxutil.F(ctx, "Post ioutil.ReadAll err:%v", err))
		return nil, err
	}
	log.Infof("respData:%v", string(respData))

	return respData, nil
}

func (c *Client) setHeader(req *http.Request, reqData []byte, now time.Time) error {
	secretId := c.cfg.SecretId
	secretKey := c.cfg.SecretKey
	host := c.cfg.Uri
	algorithm := "TC3-HMAC-SHA256"
	service := "ocr"
	version := "2018-11-19"
	action := c.cfg.Action
	region := "ap-guangzhou"
	var timestamp = now.Unix()

	// step 1: build canonical request string
	httpRequestMethod := "POST"
	canonicalURI := "/"
	canonicalQueryString := ""
	canonicalHeaders := fmt.Sprintf("content-type:application/json; charset=utf-8\nhost:%s\n", host)
	signedHeaders := "content-type;host"
	hashedRequestPayload := sha256hex(reqData)
	canonicalRequest := fmt.Sprintf("%s\n%s\n%s\n%s\n%s\n%s",
		httpRequestMethod,
		canonicalURI,
		canonicalQueryString,
		canonicalHeaders,
		signedHeaders,
		hashedRequestPayload)

	// step 2: build string to sign
	date := now.UTC().Format("2006-01-02")
	credentialScope := fmt.Sprintf("%s/%s/tc3_request", date, service)
	hashedCanonicalRequest := sha256hex([]byte(canonicalRequest))
	string2sign := fmt.Sprintf("%s\n%d\n%s\n%s",
		algorithm,
		timestamp,
		credentialScope,
		hashedCanonicalRequest)
	fmt.Println(string2sign)

	// step 3: sign string
	secretDate := hmacsha256(date, "TC3"+secretKey)
	secretService := hmacsha256(service, secretDate)
	secretSigning := hmacsha256("tc3_request", secretService)
	signature := hex.EncodeToString([]byte(hmacsha256(string2sign, secretSigning)))

	// step 4: build authorization
	authorization := fmt.Sprintf("%s Credential=%s/%s, SignedHeaders=%s, Signature=%s",
		algorithm,
		secretId,
		credentialScope,
		signedHeaders,
		signature)

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("Authorization", authorization)
	req.Header.Add("Host", host)
	req.Header.Add("X-TC-Action", action)
	req.Header.Add("X-TC-Timestamp", strconv.FormatInt(timestamp, 10))
	req.Header.Add("X-TC-Version", version)
	req.Header.Add("X-TC-Region", region)

	return nil
}

func sha256hex(s []byte) string {
	b := sha256.Sum256(s)
	return hex.EncodeToString(b[:])
}

func hmacsha256(s, key string) string {
	hashed := hmac.New(sha256.New, []byte(key))
	hashed.Write([]byte(s))
	return string(hashed.Sum(nil))
}

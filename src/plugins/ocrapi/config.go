package ocrapi

type Config struct {
	Uri       string
	UriPrefix string
	Action    string
	Version   string
	Region    string

	SecretId  string
	SecretKey string
}

func (c *Config) getPath() string {
	return c.UriPrefix + c.Uri
}

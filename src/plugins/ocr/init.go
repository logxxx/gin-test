package ocr

import (
	"common/inject"
	"plugins/aixrouter"
	"plugins/ocr/ocrbiz"
	"plugins/ocr/ocrutil"
)

const (
	// NOTE: 需要在阿波罗后台创建相应的配置
	DynConfigKey = "application/ocr"
)

func MustInit() {
	ocrutil.MustInit()

	ocrbiz.MustInit()

	// Controller
	ctr := &Controller{}
	inject.MustProvide(ctr)

	// Router
	aixrouter.RegisterRouterFunc(ctr.InitRouter)

}

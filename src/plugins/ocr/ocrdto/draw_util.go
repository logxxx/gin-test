package ocrdto

import (
	"image/color"
	"image/draw"
)

// HLine draws a horizontal line
func HLine(img draw.Image, x1, y, x2 int, color color.RGBA, thick int) {
	for ; x1 <= x2; x1++ {
		for t := 0; t < thick; t++ {
			img.Set(x1, y+t, color)
			img.Set(x1, y-t, color)
		}
	}
}

// VLine draws a veritcal line
func VLine(img draw.Image, x, y1, y2 int, color color.RGBA, thick int) {
	for ; y1 <= y2; y1++ {
		for t := 0; t < thick; t++ {
			img.Set(x+t, y1, color)
			img.Set(x-t, y1, color)
		}
	}
}

// Rect draws a rectangle utilizing HLine() and VLine()
func Rect(x1, y1, x2, y2, thick int) func(img draw.Image, color color.RGBA) {
	return func(img draw.Image, color color.RGBA) {
		HLine(img, x1, y1, x2, color, thick)
		HLine(img, x1, y2, x2, color, thick)
		VLine(img, x1, y1, y2, color, thick)
		VLine(img, x2, y1, y2, color, thick)
	}

}

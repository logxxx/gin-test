package ocrdto

import "plugins/ocr/ocrproto"

type DoGeneralOcrParams struct {
	ImgFile      string `json:"img_file"`       // 图片文件内容，见备注
	ImgUrl       string `json:"img_url"`        // 图片网络地址，见备注
	Format       string `json:"img_format"`     //　图片文件的格式，支持jpg, jpeg, png
	IsAccurateOn bool   `json:"is_accurate_on"` // 是否开启高精度识别，默认关闭。可能会降低识别速度
}

type DoGeneralOcrDto struct {
	OcrResult ocrproto.OcrResult `json:"ocr_result"`
}

var (
	ImgFileMinSize = 10 * 1024        //10kb
	ImgFileMaxSize = 10 * 1024 * 1024 //10MB

	FileFormatPNG  = "png"
	FileFormatJPG  = "jpg"
	FileFormatJPEG = "jpeg"

	DoOcrScanTypeCommon = 0 //ｏｃｒ识别类型:通用印刷体识别（高精度版）

	DoOcrStateSucc              = 0 //ｏｃｒ成功
	DoOcrStateInvaildScanType   = 1 //非法的扫描类型
	DoOcrStateEmptyImgFile      = 2 //图片为空
	DoOcrStateUnsupportedFormat = 3 //图片格式不支持
	DoOcrStateFileSizeTooSmall  = 4 //图片文件过小
	DoOcrStateFileSizeTooLarge  = 5 //图片文件过大

)

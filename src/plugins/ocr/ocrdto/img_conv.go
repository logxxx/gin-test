package ocrdto

import "encoding/base64"

//base64->图片流
func Base64ToImg(input string) ([]byte, error) {
	output, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return nil, err
	}
	return output, nil
}

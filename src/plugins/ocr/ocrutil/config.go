package ocrutil

import (
	"encoding/json"
	"plugins/aixconfig"
)

var (
	cfg = &OcrDynConfig{}
)

type OcrDynConfig struct {
	Account Account `json:"account"`
}

type Account struct {
	AppId     string `json:"app_id"`
	SecretId  string `json:"secret_id"`
	SecretKey string `json:"secret_key"`
	Region    string `json:"region"`
}

func (dc *OcrDynConfig) Load(data []byte) error {
	// TODO: 反序列化并且校验配置
	err := json.Unmarshal(data, dc)
	if err != nil {
		return err
	}
	return nil
}

func MustInit() {
	aixconfig.MustLoadPluginConfig("ocr.json", cfg)
}

func GetDynConfig() *OcrDynConfig {
	return cfg
}

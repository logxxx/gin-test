package ocrbiz_test

import (
	"common/logutil"
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"math/rand"
	"os"
	"plugins/ocr/ocrbiz"
	"plugins/ocr/ocrdto"
	"plugins/testmain"
	"testing"
	"time"
)

var CTX = context.Background()

func geneRandUid(tail int) int64 {
	//每次随机生成一个uid
	rand := (10000000+rand.Intn(9999999)+int(time.Now().UnixNano()))/100*100 + tail
	return int64(rand)
}

func TestMain(m *testing.M) {
	testmain.TestMain(m)
}

func getImageFile() []byte {
	wd, _ := os.Getwd()
	logutil.Infof("here wd:%v", wd)
	file, err := ioutil.ReadFile("src/plugins/ocr/ocrdto/pic_example/test1.jpg")
	if err != nil {
		panic(err)
	}
	return file
}

func TestOcrBiz_DoOcr(t *testing.T) {
	UID := geneRandUid(18)
	t.Logf("UID:%v", UID)

	now := time.Now()
	biz := ocrbiz.GetInstance()

	//测试一张本地图片
	imgData := getImageFile()
	req1 := &ocrdto.DoGeneralOcrParams{
		ImgFile: string(imgData),
	}
	resp1, err := biz.DoGeneralOcr(CTX, UID, req1, now)
	assert.Nil(t, err)
	assert.NotEqual(t, resp1.OcrResult.TextDetections, 0)

	//测试一张网络图片
	var imgUrl = "https://image.3001.net/images/20200402/1585818632_5e85ac082699f.jpg"
	req2 := &ocrdto.DoGeneralOcrParams{
		ImgUrl: imgUrl,
	}
	resp2, err := biz.DoGeneralOcr(CTX, UID, req2, now)
	assert.Nil(t, err)
	assert.NotEqual(t, resp2.OcrResult.TextDetections, 0)
	resStr := ""
	for i := range resp2.OcrResult.TextDetections {
		resStr += resp2.OcrResult.TextDetections[i].DetectedText + " "
	}
	t.Logf("resStr:%v", resStr)
}

func TestGetInstance(t *testing.T) {
	//生成一个用户以及对应的token

}

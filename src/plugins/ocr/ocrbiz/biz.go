package ocrbiz

import (
	"common/inject"
	"context"
	"plugins/ocr/ocrdto"
	"plugins/ocr/ocrproto"
	"plugins/ocr/ocrutil"
	"plugins/ocrapi"
	"time"
	log "common/logutil"
)

var (
	defaultBiz *OcrBiz
)

func MustInit() {
	defaultBiz = &OcrBiz{}
	inject.MustProvide(defaultBiz)
	log.Infof("here ocrbiz.MustInit()")
}

func (biz *OcrBiz) InjectInit() error {
	cfg := ocrutil.GetDynConfig()
	client, err := ocrapi.NewClient(cfg.Account.SecretId, cfg.Account.SecretKey, cfg.Account.Region)
	if err != nil {
		log.Errorf("OcrBiz InjectInit err:%v inParams:%v,%v,%v",
			err, cfg.Account.SecretId, cfg.Account.SecretKey, cfg.Account.Region)
	}
	biz.apiClient = client
	return nil
}

type OcrBiz struct {
	apiClient *ocrapi.Client
}

func GetInstance() *OcrBiz {
	return defaultBiz
}

func (biz *OcrBiz) DoGeneralOcr(ctx context.Context, uid int64, p *ocrdto.DoGeneralOcrParams, now time.Time) (*ocrdto.DoGeneralOcrDto, error) {
	resp := &ocrdto.DoGeneralOcrDto{}
	apiResp, apiErr, err := biz.apiClient.GeneralOCR(ctx, []byte(p.ImgFile), p.ImgUrl, p.IsAccurateOn, now)
	if err != nil {
		return nil, err
	}
	if apiErr != nil {
		resp.OcrResult.Error.Code = apiErr.Response.Error.Code
		resp.OcrResult.Error.Message = apiErr.Response.Error.Message
		resp.OcrResult.RequestId = apiErr.Response.RequestId
		return resp, nil
	}

	for i := range apiResp.TextDetections {
		tmp := apiResp.TextDetections[i]
		elem := ocrproto.TextDetections{
			DetectedText: tmp.DetectedText,
			Confidence:   tmp.Confidence,
			ItemPolygon: ocrproto.ItemPolygon{
				X:      tmp.ItemPolygon.X,
				Y:      tmp.ItemPolygon.Y,
				Width:  tmp.ItemPolygon.Width,
				Height: tmp.ItemPolygon.Height,
			},
		}
		resp.OcrResult.TextDetections = append(resp.OcrResult.TextDetections, elem)
	}
	resp.OcrResult.RequestId = apiResp.RequestId
	resp.OcrResult.Angel = apiResp.Angel
	return resp, nil
}

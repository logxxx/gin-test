package ocr

import (
	log "common/logutil"
	reqresp "common/reqresputil"
	"context"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"plugins/ocr/ocrbiz"
	"plugins/ocr/ocrdto"
	"plugins/ocr/ocrproto"
	"plugins/ocrapi"
	"time"
)

type Controller struct {
	OcrBiz  *ocrbiz.OcrBiz          `inject:""`
}

// ocr > 通用文字识别
func (ctr *Controller) DoGeneralOcr(ctx context.Context, req *ocrproto.DoGeneralOcrReq) (*ocrproto.DoGeneralOcrResp, error) {
	params := &ocrdto.DoGeneralOcrParams{
		ImgFile:      req.ImgFile,
		ImgUrl:       req.ImgUrl,
		IsAccurateOn: req.IsAccurateOn,
	}
	dto, err := ctr.OcrBiz.DoGeneralOcr(ctx, 0, params, time.Now())
	resp := &ocrproto.DoGeneralOcrResp{}
	if dto != nil {
		resp.OcrResult = dto.OcrResult
	}
	return resp, err
}

type GetTokenReq struct {
	reqresp.Request
	Uid int64 `json:"in"`
}

type GetTokenResp struct {
	reqresp.Response
	Token string `json:"out"`
}

func (ctr *Controller) UploadForTest(c *gin.Context) {
	ctx := reqresp.GetContext(c)
	file, err := readFile(c, "file")
	if err != nil {
		log.Errorf("UploadForTest readFile err:%v", err)
		reqresp.DoReturn(c, nil, err)
	}
	log.Infof("UploadForTest file len:%v", len(file))

	format := c.PostForm("format")

	log.Infof("UploadForTest file format:%v", format)

	params := &ocrdto.DoGeneralOcrParams{
		ImgFile: string(file),
		Format:  format,
	}
	dto, err := ctr.OcrBiz.DoGeneralOcr(ctx, 0, params, time.Now())
	resp := &ocrproto.DoGeneralOcrResp{}
	if dto != nil {
		resp.OcrResult = dto.OcrResult
	}

	res, err := ocrapi.GeneImg(ctx, params, dto)
	if err != nil {
		log.Errorf("UploadForTest GeneImg err:%v", err)
		reqresp.DoReturn(c, nil, err)
	}

	c.Data(http.StatusOK, "image/jpg", res)
	c.Header("Content-Type", "image/jpg")

	//如果要下载文件，用这个:
	//fileContentDisposition := fmt.Sprintf(`attachment; filename="%v.jpg"`, url.QueryEscape(time.Now().Format("150405")))
	//c.Header("Content-Disposition", fileContentDisposition)

}

func readFile(ginc *gin.Context, filename string) ([]byte, error) {
	formHeader, err := ginc.FormFile(filename)
	if err != nil {
		return nil, err
	}
	formFile, err := formHeader.Open()
	if err != nil {
		return nil, err
	}
	defer formFile.Close()
	data, err := ioutil.ReadAll(formFile)
	if err != nil {
		return nil, err
	}
	return data, nil
}

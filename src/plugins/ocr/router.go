package ocr

import (
	log "common/logutil"
	"github.com/gin-gonic/gin"
	"os"
	"plugins/ocr/ocrproto"
)

func (ctr *Controller) InitRouter(r gin.IRouter, prefix string) {
	// ocr > 通用文字识别
	ocrproto.RegisterDoGeneralOcrHandler(r, prefix, ctr.DoGeneralOcr)
	r.POST("ocr/upload_for_test", ctr.UploadForTest)
	wd, _ := os.Getwd()
	log.Infof("InitRouter wd:%v", wd)
	r.StaticFile("ocr/test.html", "src/plugins/ocr/frontend/index.html")
}

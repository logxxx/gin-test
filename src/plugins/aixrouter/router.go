package aixrouter

import "github.com/gin-gonic/gin"

type RouteRegisterFunc func(r gin.IRouter, prefix string)

type router struct {
	items []RouteRegisterFunc
}

var (
	defaultRouter = &router{}
)

func RegisterRouterFunc(fn RouteRegisterFunc) {
	defaultRouter.items = append(defaultRouter.items, fn)
}

func RegisterRoute(r gin.IRouter, prefix string) {
	for _, i := range defaultRouter.items {
		i(r, prefix)
	}
}

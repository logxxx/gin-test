package mytccdto

import "errors"

var (
	ServerError = errors.New("server error")
	ErrInsufficientMoney = errors.New("insufficient money")
	ErrItsTooLateToAbort = errors.New("its too late to abort")
	ErrDuplicateAbortOpt = errors.New("duplicate abort option")
	ErrGroupNotFound = errors.New("group not found")
)
package mytccbiz

type Member struct {
	Uid int64
	CurrencyType string
	Delta int
	LastExecErr error //保存最近一次的错误
	State int //0:未处理 1:处理成功 2:处理失败
}

var (
	MemberStateUnDeal = 0
	MemberStateSucc = 1
	MemberStateFailed = 2
)
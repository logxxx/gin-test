package mytccbiz

import (
	"plugins/mytcc/mytccdao"
	"plugins/mytcc/mytccdto"
)

var _ MyTcc = &MyTccDefault{} //编译期检查, 是否实现了接口

type MyTcc interface {
	GeneCommitId() (string, error)
	UpdateBalance(commitId string, uid int64, currencyType string, delta int) error
	Commit(commitId string) error
	Abort(commitId string) error
	GetBalance(uid int64, currencyType string) (int, error)
}

type MyTccDefault struct {
	groupMgr GroupManager
}

func NewTcc() MyTcc {
	return &MyTccDefault{groupMgr:NewGroupManager()}
}

func (m *MyTccDefault) GeneCommitId() (string, error) {
	group, err := m.groupMgr.CreateGroup()
	if err != nil {
		return "", err
	}
	if group == nil { //不应出现此情况
		return "", mytccdto.ServerError
	}
	return group.GroupId, nil
}

func (m *MyTccDefault) Commit(commitId string) error {
	err := m.groupMgr.Commit(commitId)
	if err != nil {
		return err
	}

	return nil
}

func (m *MyTccDefault) Abort(commitId string) error {
	err := m.groupMgr.Abort(commitId)
	if err != nil {
		return err
	}

	return nil
}

func (m *MyTccDefault) GetBalance(uid int64, currencyType string) (int, error) {
	//先从db查余额
	balance, err := mytccdao.GetDBInstance().GetBalance(uid, currencyType)
	if err != nil {
		return -1, err
	}

	//获取中间件中所有预扣的钱
	reducedTotal, err := m.groupMgr.GetMoneyReducedByTcc(currencyType)
	if err != nil {
		return -1, err
	}

	//实际能用的钱=db的钱-中间件预扣的钱
	return balance - reducedTotal, nil
}

func (m *MyTccDefault) UpdateBalance(commitId string, uid int64, currencyType string, delta int) error {
	if delta == 0 {
		return nil
	}

	//如果是扣钱，提前检查是否够扣
	if delta < 0 {
		balance, err := m.GetBalance(uid, currencyType)
		if err != nil {
			return err
		}
		if balance + delta < 0 {
			return mytccdto.ErrInsufficientMoney
		}
	}

	group, err := m.groupMgr.MustGetGroup(commitId)
	if err != nil {
		return err
	}
	if group != nil { //不应出现此情况
		return mytccdto.ServerError
	}

	member := &Member{
		Uid:          uid,
		CurrencyType: currencyType,
		Delta:        delta,
		State:        MemberStateUnDeal,
	}
	err = group.AddMember(member)
	if err != nil {
		return err
	}

	return nil
}

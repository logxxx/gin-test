package mytccbiz

import (
	"plugins/mytcc/mytccdao"
	"plugins/mytcc/mytccdto"
)

type Group struct {
	GroupId string        //即对外的commitId
	State int             //组的状态
	Members []Member      //组员，通过group.AddMember()添加
	FailedMemberCount int //处理失败的组员数量
	ExecCount int         //执行次数(组的维度)
}

var (
	GroupStateAborted = -1 //被废弃
	GroupStateUnDeal = 0 //未执行过
	GroupStateSucc = 1 //执行成功
	GroupStateFailed = 2 //执行失败(可能是部分失败)
)

func (g *Group) Exec() error {
	//不执行已废弃的组;不执行已全部成功的组
	if g.State == GroupStateAborted || g.State == GroupStateSucc {
		return nil
	}

	db := mytccdao.GetDBInstance()
	for i := range g.Members {
		tmp := g.Members[i]
		//不执行已成功的成员
		if tmp.State == MemberStateSucc {continue}
		err := db.UpdateBalance(tmp.Uid, tmp.CurrencyType, tmp.Delta)
		if err != nil {
			g.FailedMemberCount++
			tmp.State = MemberStateFailed
			tmp.LastExecErr = err
		} else {
			tmp.State = MemberStateSucc
		}
	}

	//组的执行次数+1
	g.ExecCount++

	//TODO:这里有个非常重要的问题，执行失败了怎么办???
	return nil
}

func(g *Group) AddMember(member *Member) error {
	return nil
}

func(g *Group) Abort() error {
	if g.State == GroupStateSucc || g.State == GroupStateFailed {
		return mytccdto.ErrItsTooLateToAbort
	}
	if g.State == GroupStateAborted {
		return mytccdto.ErrDuplicateAbortOpt
	}
	g.State = GroupStateAborted
	return nil
}

//获取组内所有预扣的金额
func(g *Group) GetReducedMoney(currencyType string) (int, error) {
	reducedMoney := 0
	for i := range g.Members {
		tmp := g.Members[i]
		//需要满足条件:1.是扣钱 2.币种能对上 3.还未执行成功，即包括未执行的+执行了但失败了的；不包括已执行成功+废弃了的
		if tmp.Delta < 0 && tmp.CurrencyType == currencyType && tmp.State != MemberStateSucc {
			reducedMoney += tmp.Delta
		}
	}
	return reducedMoney, nil
}
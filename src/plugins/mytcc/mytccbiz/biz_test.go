package mytccbiz_test

import (
	"plugins/mytcc/mytccbiz"
	"testing"
)

func TestNewTcc(t *testing.T) {
	//测试用例:正常流程

	tcc := mytccbiz.NewTcc()
	if tcc == nil {
		t.Fatal()
	}
	commitId, err := tcc.GeneCommitId()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("commitId:%v", commitId)
}

func TestMain(m *testing.M) {

}
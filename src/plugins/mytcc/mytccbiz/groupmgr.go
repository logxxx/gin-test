package mytccbiz

import (
	"plugins/mytcc/mytccdao"
	"plugins/mytcc/mytccdto"
	"time"
)

type GroupManager interface {
	CreateGroup() (*Group, error)
	MustGetGroup(groupId string) (*Group, error)
	GetMoneyReducedByTcc(currencyType string) (int, error)
	Commit(groupId string) error
	Abort(groupId string) error
}

func NewGroupManager() GroupManager {
	return &GroupManagerDefault{
		groups: make([]Group, 0),
	}
}

type GroupManagerDefault struct {
	groups []Group //暂时用slice实现。有分布式问题，后面改造成用redis实现
}

func (g *GroupManagerDefault) CreateGroup() (*Group, error) {
	groupId, err := mytccdao.GetRedisInstance().NewGroupId()
	if err != nil {
		return nil, err
	}
	newGroup := Group{
		GroupId:           groupId,
		State:             GroupStateUnDeal,
		Members:           make([]Member, 0),
		FailedMemberCount: 0,
		ExecCount:         0,
	}

	if g.groups == nil {
		g.groups = make([]Group, 0)
	}

	g.groups = append(g.groups, newGroup)

	//1分钟后定时废弃
	go func() {
		c := time.Tick(1*time.Minute)
		for {
			<-c
			group, err := g.MustGetGroup(groupId)
			if err != nil {
				return
			}
			if group == nil {
				return
			}
			group.Abort()
		}
	}()

	return &newGroup, nil
}

func (g *GroupManagerDefault) MustGetGroup(groupId string) (*Group, error) {
	for i := range g.groups {
		if g.groups[i].GroupId == groupId {
			return &g.groups[i], nil
		}
	}
	return nil, mytccdto.ErrGroupNotFound
}


func (g *GroupManagerDefault) GetMoneyReducedByTcc(currencyType string) (int, error) {
	reducedMoneyTotal := 0
	for i := range g.groups {
		delta, err := g.groups[i].GetReducedMoney(currencyType)
		if err != nil {
			return -1, err
		}
		reducedMoneyTotal += delta
	}
	return reducedMoneyTotal, nil
}



func (g *GroupManagerDefault) Commit(groupId string) error {
	group, err := g.MustGetGroup(groupId)
	if err != nil {
		return err
	}
	if group == nil { //不应出现此情况
		return mytccdto.ServerError
	}
	err = group.Exec()
	if err != nil {
		return err
	}

	return nil
}

func (g *GroupManagerDefault) Abort(groupId string) error {
	group, err := g.MustGetGroup(groupId)
	if err != nil {
		return err
	}
	if group == nil { //不应出现此情况
		return mytccdto.ServerError
	}
	err = group.Abort()
	if err != nil {
		return err
	}

	return nil
}
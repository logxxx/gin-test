package mytccdao

import "errors"

type myDB struct {

}

//出参1:rowAffected
func (m *myDB) UpdateBalance(uid int64, currencyType string, delta int) (error) {
	return nil
}

//出参1: balance
func (m *myDB) GetBalance(uid int64, currencyType string) (int, error) {
	return  100, nil
}

func GetDBInstance() *myDB {
	return &myDB{}
}

var (
	DbErrorNoRowsAffected = errors.New("no rows affected")
)
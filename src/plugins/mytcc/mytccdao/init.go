package mytccdao

import (
	"common/inject"
	"gopkg.in/redis.v5"
)

var (
	dao = &TccRedisDao{}
)

type TccRedisDao struct {
	Redis *redis.Client `inject:"@redis-main"`
}

func (dao *TccRedisDao) Close() error {
	return dao.Redis.Close()
}

func MustInit() {
	inject.MustProvide(dao)
}
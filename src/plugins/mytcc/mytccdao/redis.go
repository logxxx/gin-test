package mytccdao

import (
	"fmt"
	"time"
)

var (
	keyGeneratorGroupID string = "tcc:group:id:%s" // 生成GroupId 自增键，每天按小时为key自增
	keyGeneratorGroupMgrID string = "tcc:groupmgr:id:%s" // 生成GroupManagerId 自增键，每天按小时为key自增
)

func GetRedisInstance() *TccRedisDao {
	return dao
}

func (t *TccRedisDao) NewGroupId() (string, error) {
	// 精确到小时
	dtHourKey := time.Now().Format("010215")
	groupIdKey := fmt.Sprintf(keyGeneratorGroupID, dtHourKey)
	inc, err := t.Redis.Incr(groupIdKey).Result()
	if err != nil {
		return "", err
	}
	err = t.Redis.Expire(groupIdKey, 24*time.Hour).Err()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s-%s-%03d","g", dtHourKey, inc), nil
}

func (t *TccRedisDao) NewGroupMgrId() (string, error) {
	// 精确到小时
	dtHourKey := time.Now().Format("010215")
	mgrIdKey := fmt.Sprintf(keyGeneratorGroupMgrID, dtHourKey)
	inc, err := t.Redis.Incr(mgrIdKey).Result()
	if err != nil {
		return "", err
	}
	err = t.Redis.Expire(mgrIdKey, 24*time.Hour).Err()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s-%s-%03d","gmgr", dtHourKey, inc), nil
}
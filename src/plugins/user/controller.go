package user

import (
	"common/logutil"
	"common/reqresputil"
	"github.com/gin-gonic/gin"
	"log"
	"plugins/user/userbiz"
	"plugins/user/userdto"
	"time"
)

type DoCreateUserReq struct {
	reqresputil.RequestCommon
	Nickname string `json:"nick_name"`
}

type DoCreateUserResp struct {
	reqresputil.Response
	Uid int64 `json:"uid"`
}

func DoCreateUser(c *gin.Context) {
	req := &DoCreateUserReq{}
	resp := &DoCreateUserResp{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		logutil.Errorf("DoCreateUser_ParseBody_err:%v req:%#v", err, req)
		reqresputil.DoReturn(c, resp, err)
		return
	}
	log.Printf("DoCreateUser Req:%#v", req)

	userBiz := userbiz.GetInstance()
	p := &userdto.CreateUserParams{Nickname: req.Nickname}
	dto, err := userBiz.CreateUser(c.Request.Context(), p, time.Now())
	if dto != nil {
		resp.Uid = dto.Uid
	}
	reqresputil.DoReturn(c, resp, err)
}

type GetUsersReq struct {
	reqresputil.Request
}

type GetUsersResp struct {
	reqresputil.Response
	Users []userdto.UserInfo `json:"users"`
}

func GetUsers(c *gin.Context) {
	req := &GetUsersReq{}
	resp := &GetUsersResp{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		logutil.Errorf("GetUsers_ParseBody_err:%v req:%#v", err, req)
		reqresputil.DoReturn(c, resp, err)
		return
	}

	userBiz := userbiz.GetInstance()
	p := &userdto.GetUsersParams{}
	dto, err := userBiz.GetUsers(c.Request.Context(), p, time.Now())
	if dto != nil {
		resp.Users = dto.Users
	}
	reqresputil.DoReturn(c, resp, err)
}

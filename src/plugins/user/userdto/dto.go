package userdto

import "common/dto"

type GetUsersParams struct {
	JustNeedToken bool
}

type GetUsersDto struct {
	Users []UserInfo `json:"users"`
}

type CreateUserParams struct {
	Nickname string `json:"nickname"`
}

type CreateUserDto struct {
	Uid int64
	Nickname string
}

type UserInfo struct {
	Uid int64 `json:"UID"`
	UidStr string
	Token string `json:"TOKEN"`
	ApiGetUserInfoResp
}

type ApiGetUserInfoReq struct {
	Common dto.ApiReqCommon `json:"common"`
}

type ApiGetUserInfoResp struct {
	UserInfo struct {
		Uid string `json:"uid"`
		NickName string `json:"nickname"`
		YdbBalance int `json:"ydb_balance"`
		TodayYdb int `json:"today_ydb"`
		YdbFromFriends int `json:"ydb_from_friends"`
		YdbHistoryFromFriends int `json:"ydb_history_form_friends"`
		RevenueBalance int `json:"revenue_balance"`
	} `json:"user"`
}

const (
	UidRange = 999999999
	UidStart = 100000000

	GetUserInfoStateSucc = 0
	GetUserInfoStateNoExist = 1
)
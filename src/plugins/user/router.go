package user

import "github.com/gin-gonic/gin"

func InitRouter(r gin.IRouter, prefix string) {
	r.POST(prefix+"/user/do_create_user", DoCreateUser)
	r.POST(prefix+"/user/get_users", GetUsers)
}
package userdao

import (
	"common/randutil"
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"plugins/user/userdto"
	"time"
)

//CREATE TABLE user_info (
//	id BIGINT unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
//	uid BIGINT NOT NULL COMMENT '用户id',
//	nickname varchar(32) NOT NULL COMMENT '用户昵称',
//	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
//	mtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
//	last_login_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登录时间',
//	PRIMARY KEY (id),
//	UNIQUE KEY (uid)
//)ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT '用户信息表';

func geneUid() int64 {
	r := randutil.NewRand()
	return r.Int63n(userdto.UidRange) + userdto.UidStart
}

func (d *UserDao) CreateUser(ctx context.Context, p *userdto.CreateUserParams, now time.Time) (*userdto.CreateUserDto, error) {
	sqlInsert := "INSERT INTO user_info (uid, nickname) VALUES(?,?);"
	randomUid := geneUid()
	_, err := d.Mysql.ExecContent(ctx, sqlInsert, randomUid, p.Nickname)
	if err != nil {
		return nil, err
	}
	return &userdto.CreateUserDto{
		Uid: randomUid,
		Nickname: p.Nickname,
	}, nil
}

type TableUserInfo struct {
	Id            int64     `json:"id"`
	Uid           int64     `json:"uid"`
	Nickname      string    `json:"nickname"`
	LastLoginTime time.Time `json:"last_login_time"`
	CreateTime    time.Time `json:"ctime"`
	UpdateTime    time.Time `json:"mtime"`
}

func (d *UserDao) GetUserInfo(ctx context.Context, uid int64, now time.Time) (bool, *TableUserInfo, error) {
	sqlQuery := "SELECT id, uid, nickname, last_login_time, ctime, mtime " +
		"FROM user_info WHERE uid = ?"
	userInfo := &TableUserInfo{}
	err := sqlx.GetContext(ctx, d.Mysql.GetDB(), userInfo, sqlQuery, uid)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil, nil
		}
		return false, nil, err
	}

	return true, userInfo, nil

}
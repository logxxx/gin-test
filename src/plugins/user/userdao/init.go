package userdao

import (
	"common/inject"
	"plugins/connection/connectiondao/mysqlclient"
)

var (
	dao = &UserDao{}
)

type UserDao struct {
	Mysql *mysqlclient.MysqlClient `inject:"@mysql-main"`
}

func MustInit() {
	inject.MustProvide(dao)
}

func (d *UserDao) Close() error {
	return nil
}
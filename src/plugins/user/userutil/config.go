package userutil

import (
	"plugins/aixconfig"
)

var (
	cfg = &Config{}
)

type Config struct {
	Users []UserConfig `json:"users"`
}

type UserConfig struct {
	IsMain bool `json:"is_main"`
	Uid int64 `json:"uid"`
	Token string `json:"token"`
}

func MustInit() {
	aixconfig.MustLoadPluginConfig("user.json", cfg)
}

func GetConfig() *Config {
	return cfg
}
package userbiz

import (
	"common/dto"
	"common/logutil"
	"context"
	"plugins/user/userdto"
	"plugins/user/userutil"
	"strconv"
	"time"
)


func (biz *UserBiz) GetUsers(ctx context.Context, p *userdto.GetUsersParams, now time.Time) (*userdto.GetUsersDto, error) {
	cfg := userutil.GetConfig()

	path := dto.DomainCmwayx + "/cmway/user/get_info"

	respUsers := make([]userdto.UserInfo, 0 )
	for i := range cfg.Users {
		uid := cfg.Users[i].Uid
		token := cfg.Users[i].Token
		apiReq := &userdto.ApiGetUserInfoReq{}
		apiReq.Common.Uid = strconv.FormatInt(uid, 10)
		apiReq.Common.Token = token
		resp := userdto.UserInfo{
			Uid: uid,
			Token: token,
			UidStr: strconv.FormatInt(uid, 10),
		}

		if !p.JustNeedToken {
			apiResp := userdto.ApiGetUserInfoResp{}
			err := biz.HttpClient.Post(ctx, path, apiReq, &apiResp)
			if err != nil {
				logutil.Errorf("GetUserInfo_Post_err:%v req:%#v", err, apiReq)
				return nil, err
			}
			resp.ApiGetUserInfoResp = apiResp
		}

		respUsers = append(respUsers, resp)
	}


	resp := &userdto.GetUsersDto{}
	resp.Users = respUsers
	return resp, nil
}

func (biz *UserBiz) CreateUser(ctx context.Context, p *userdto.CreateUserParams, now time.Time) (*userdto.CreateUserDto, error) {
	return biz.UserDao.CreateUser(ctx, p, now)
}
package userbiz

import (
	"common/inject"
	"plugins/connection/connectionbiz/httpclient"
	"plugins/user/userdao"
)

var (
	biz = &UserBiz{}
)

type UserBiz struct {
	UserDao *userdao.UserDao `inject:""`
	HttpClient *httpclient.HttpClient `inject:"httpclient"`
}

func MustInit()  {
	inject.MustProvide(biz)
}

func GetInstance() *UserBiz {
	return biz
}
package user

import (
	"plugins/aixrouter"
	"plugins/user/userbiz"
	"plugins/user/userdao"
	"plugins/user/userutil"
)

func MustInit() {
	userutil.MustInit()
	userdao.MustInit()
	userbiz.MustInit()
	aixrouter.RegisterRouterFunc(InitRouter)
}
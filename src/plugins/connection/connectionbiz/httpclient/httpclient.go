package httpclient

import (
	"bytes"
	"common/inject"
	"common/logutil"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"time"
)

const (
	ErrTypeUnknown ErrType = iota
	ErrTypeBadRequest
	ErrRpcError
	ErrTypeBadResponse
	ErrTypeTimeout
)

var _ error = &RpcError{}
var httpClient *HttpClient

type ErrType int

type RpcError struct {
	Code int
	Type ErrType
	Err  error
}

type Resp struct {
	Ret int
	Msg string

	Data interface{}
}

type HttpClient struct {
	client *http.Client
}

func MustInit() {
	httpClient = &HttpClient{
		client: &http.Client{
			Timeout: time.Millisecond * time.Duration(1000),
		},
	}
	inject.MustProvideByName("httpclient", httpClient)
}

func (r *HttpClient) InvokeRaw(ctx context.Context, path string, method string, req interface{}, out interface{}) error {
	var reqData []byte
	var respBodyData []byte
	var statusCode int

	defer func() {
		if logutil.EnableLogFor(logutil.DEBUG) {
			logutil.Debugf("[%s %s]code=%d req=%#v resp=%#v",
				method,  path, statusCode, string(reqData), string(respBodyData))
		}
	}()

	err := func() *RpcError {
		var err error
		var httpResp *http.Response
		var body io.Reader = nil

		// 准备请求
		if req != nil {
			reqData, err = json.Marshal(req)
			if err != nil {
				return newRpcError(statusCode, ErrTypeBadRequest, err)
			}
			body = bytes.NewBuffer(reqData)
		}

		req, err := http.NewRequest(method, path, body)
		if err != nil {
			return newRpcError(statusCode, ErrTypeBadRequest, err)
		}
		req.Header.Set("Content-Type", "application/json")
		if ctx != nil {
			req = req.WithContext(ctx)
		}

		// 发起RPC
		httpResp, err = r.client.Do(req)
		if err != nil {

			if realErr, ok := err.(net.Error); ok && realErr.Timeout() {
				return newRpcError(statusCode, ErrTypeTimeout, realErr)
			}

			if realErr, ok := err.(*url.Error); ok && realErr.Err == context.DeadlineExceeded {
				return newRpcError(statusCode, ErrTypeTimeout, err)
			}

			return newRpcError(statusCode, ErrRpcError, err)
		}
		defer httpResp.Body.Close()

		statusCode = httpResp.StatusCode
		if statusCode >= 400 {
			return newRpcError(statusCode, ErrTypeBadResponse, err)
		}

		// 处理结果
		respBodyData, err = ioutil.ReadAll(httpResp.Body)
		if err != nil {
			return newRpcError(statusCode, ErrTypeBadResponse, err)
		}

		err = json.Unmarshal(respBodyData, out)
		if err != nil {
			return newRpcError(statusCode, ErrTypeBadResponse, err)
		}
		return nil
	}()

	if err != nil {
		logutil.Errorf("[%s %s]fail:code=%d req=%#v resp=%#v err='%s'",
			method, path, statusCode, req, string(respBodyData), err)
		return err
	}

	return nil
}

func (r *HttpClient) Post(ctx context.Context, path string, req interface{}, out interface{}) error {
	e := r.InvokeRaw(ctx, path, "POST", req, out)
	return e
}

func (r *HttpClient) Get(ctx context.Context, path string, req interface{}, out interface{}) error {
	e := r.InvokeRaw(ctx, path, "GET", req, out)
	return e
}

func (r *HttpClient) Close() error {
	return nil
}

func newRpcError(c int, t ErrType, err error) *RpcError {
	return &RpcError{Code: c, Type: t, Err: err}
}

func (e *RpcError) Error() string {
	return fmt.Sprintf("rpc error: code=%d type=%d err=(%T)'%s'", e.Code, e.Type, e.Err, e.Err)
}

func ConvertToRpcError(err error) *RpcError {
	if err == nil {
		return nil
	}

	if e, ok := err.(*RpcError); ok {
		return e
	}
	return &RpcError{Code: 0, Type: ErrTypeUnknown, Err: err}
}

func IsError(err error, errType ErrType) bool {
	if ConvertToRpcError(err).Type == errType {
		return true
	}
	return false

}

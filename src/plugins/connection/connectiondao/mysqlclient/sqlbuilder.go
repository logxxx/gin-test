package mysqlclient

import (
	"context"
	"database/sql"
	"reflect"
	"strings"
)

type SQLBuilder interface {
	GetSQL() string
	GetParams() []interface{}
}

type InsertBuilder struct {
	sql string
	params []interface{}
}

func (b *InsertBuilder) GetSQL() string {
	return b.sql
}

func (b *InsertBuilder) GetParams() []interface{} {
	return b.params
}

func (c *MysqlClient) GetInsertBuilder(table string, model interface{}, ignoreFields ...string) SQLBuilder {
	fieldMap := c.db.Mapper.TypeMap(reflect.TypeOf(model))
	valueMap := c.db.Mapper.FieldMap(reflect.ValueOf(model).Elem())

	sql := "INSERT INTO `" + table + "`("

	var params []interface{}
	var placeHolders []string
	var columnNames []string

	for _, v := range fieldMap.Index {
		name := v.Name
		if data, ok := valueMap[name]; ok {
			placeHolders = append(placeHolders, "?")
			columnNames = append(columnNames, name)
			params = append(params, data.Interface())
		}
	}

	sql += strings.Join(columnNames, ",") + ")" +
		"VALUES(" + strings.Join(placeHolders, ",") + ")"

	builder := &InsertBuilder{
		sql: sql,
		params: params,
	}
	return builder
}

func (c *MysqlClient) ExecContent(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return c.db.ExecContext(ctx, query, args...)
}
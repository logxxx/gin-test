package mysqlclient

import (
	"common/inject"
	"errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/reflectx"
	"plugins/aixconfig"
	"plugins/connection/connectiondto"
	"strings"
	"time"
)

type MysqlClient struct {
	name string
	db *sqlx.DB
}

func MustInit() {
	mysql, err := NewMysql()
	if err != nil {
		panic(err)
	}
	inject.MustProvideByName("mysql.main", mysql)
}

func NewMysql() (*MysqlClient, error) {
	cfg := &connectiondto.MySqlConfig{}
	aixconfig.MustLoadPluginConfig("mysql.json", cfg)
	db, err := sqlx.Open("mysql", cfg.DataSourceName)
	if err != nil {
		return  nil, err
	}
	if db == nil {
		return nil, errors.New("db==nil")
	}
	db.SetMaxOpenConns(cfg.MaxOpenConnections)
	db.SetMaxIdleConns(cfg.MaxIdleConnections)
	db.SetConnMaxLifetime(time.Second*time.Duration(cfg.MaxLifeTime))

	db.Mapper = reflectx.NewMapperFunc("json", strings.ToLower)

	name := "DB<unknown>"
	if cfg, err := mysql.ParseDSN(cfg.DataSourceName); err == nil {
		name = fmt.Sprintf("DB<%s/%s>", cfg.Addr, cfg.DBName)
	}

	client := &MysqlClient{
		name: name,
		db: db,
	}

	return client, nil
}

func (c *MysqlClient) Close() error {
	return c.db.Close()
}

func (c *MysqlClient) GetDB() *sqlx.DB {
	return c.db
}
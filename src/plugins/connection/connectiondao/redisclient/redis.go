package redisclient

import (
	"common/inject"
	"gopkg.in/redis.v5"
	"plugins/aixconfig"
	"plugins/connection/connectiondto"
	"time"
)

var (
	client = &redis.Client{}
)

func MustInit() {
	cfg := &connectiondto.RedisConfig{}
	aixconfig.MustLoadPluginConfig("redis.json", cfg)
	client = redis.NewClient(&redis.Options{
		Addr: cfg.Addr,
		Password: cfg.Password,
		DB: 1,
		IdleTimeout: time.Second * 60,
	})

	inject.MustProvideByName("redis.main", client)
}
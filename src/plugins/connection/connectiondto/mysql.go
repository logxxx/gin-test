package connectiondto

type MySqlConfig struct {
	DataSourceName string `json:"data_source_name"`
	MaxOpenConnections int `json:"max_open_connections"`
	MaxIdleConnections int `json:"max_idle_connections"`
	MaxLifeTime int `json:"max_life_time"`
}

type RedisConfig struct {
	Addr string `json:"addr"`
	Password string `json:"password"`
}
package connection

import (
	"plugins/connection/connectionbiz/httpclient"
	"plugins/connection/connectiondao/mysqlclient"
	"plugins/connection/connectiondao/redisclient"
)

func MustInit() {
	httpclient.MustInit()
	mysqlclient.MustInit()
	redisclient.MustInit()
}
package novice

import "plugins/novice/novicebiz"

func MustInit() {
	novicebiz.MustInit()
}

package novicedto

import (
	"common/dto"
	"encoding/json"
)

type ApiListResp struct {
	Common dto.ApiRespCommon `json:"resp_common"`
	Tasks []noviceTask `json:"tasks"`
}

type noviceTask struct {
	ID          string           `json:"id"`
	Type        string           `json:"type"`
	BonusType   string           `json:"bonus_type"`
	BonusAmount int              `json:"bonus_amount"`
	ActionAttr  *json.RawMessage `json:"action_attr"`
}

type ApiCompleteReq struct {
	Common dto.ApiReqCommon `json:"common"`
	TaskID      string `json:"task_id"`
}
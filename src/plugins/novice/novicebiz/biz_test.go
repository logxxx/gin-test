package novicebiz_test

import (
	"context"
	"plugins/novice/novicebiz"
	"plugins/testmain"
	"plugins/user/userdto"
	"testing"
	"time"
)

func TestNoviceBiz_GetList(t *testing.T) {
	ctx := context.Background()
	now := time.Now()
	biz := novicebiz.GetInstance()
	req1 := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	resp1, err := biz.UserBiz.GetUsers(ctx, req1, now)
	if err != nil {
		t.Fatal(err)
	}
	if resp1 == nil {
		t.Fatal()
	}
	user := resp1.Users[0]
	resp2, err := biz.GetList(ctx, user.UidStr, user.Token, now)
	if err != nil {
		t.Fatal(err)
	}
	if resp2 == nil {
		t.Fatal()
	}
	for i := range resp2.Tasks {
		t.Logf("%#v", resp2.Tasks[i])
		err := biz.DoComplete(ctx, user.UidStr, user.Token, resp2.Tasks[i].ID, now)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("ok")
		time.Sleep(2*time.Second)
	}
}

func TestMain(m *testing.M) {
	testmain.TestMain(m)
}
package novicebiz

import (
	"common/dto"
	"common/inject"
	"common/logutil"
	"context"
	"plugins/connection/connectionbiz/httpclient"
	"plugins/novice/novicedto"
	"plugins/user/userbiz"
	"time"
)

var (
	biz = &NoviceBiz{}
)

type NoviceBiz struct {
	UserBiz *userbiz.UserBiz `inject:""`
	HttpClient *httpclient.HttpClient `inject:"httpclient"`
}

func MustInit()  {
	inject.MustProvide(biz)
}

func GetInstance() *NoviceBiz {
	return biz
}

func (biz *NoviceBiz) DoComplete(ctx context.Context, uid, token string, taskId string, now time.Time) error {
	path := dto.DomainCmwayx + "/cmway/task/novice/complete"
	apiReq := &novicedto.ApiCompleteReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiReq.TaskID = taskId
	apiResp := &dto.ApiResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, apiResp)
	if err != nil {
		logutil.Errorf("DoComplete_Post_err:%v req:%#v", err, apiReq)
		return err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("DoComplete_Post_err:%v req:%#v resp.Common:%#v",
			"apiResp.Common.Ret != dto.ApiRespRetOK", apiReq, apiResp.Common)
		return err
	}
	return nil
}

func (biz *NoviceBiz) GetList(ctx context.Context, uid, token string, now time.Time) (*novicedto.ApiListResp, error) {
	path := dto.DomainCmwayx + "/cmway/task/novice"
	apiReq := &dto.ApiReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiResp := &novicedto.ApiListResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, apiResp)
	if err != nil {
		logutil.Errorf("GetList_Post_err:%v req:%#v", err, apiReq)
		return nil, err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetList_Post_err:%v req:%#v resp.Common:%#v",
			"apiResp.Common.Ret != dto.ApiRespRetOK", apiReq, apiResp.Common)
		return nil, err
	}
	return apiResp, nil
}

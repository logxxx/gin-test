package taskdto

import "common/dto"

type ApiGetTasksResp struct {
	Common dto.ApiRespCommon `json:"resp_common"`
	Tasks []TaskInfo `json:"tasks"`
}

type TaskInfo struct {
	ID          string `json:"task_id"`
	Type        string `json:"type"`
	Requirement struct {
		MinPlayTime int `json:"min_play_time"` // 单位秒
	} `json:"requirement"`
	TaskAttr struct {
		MaxExecCount int              `json:"max_exec_count"`
		LimitType    int              `json:"limit_type"`
		BonusType    string           `json:"bonus_type"`
		BonusAmount  int              `json:"bonus_amount"` // 奖励金额，单位为分或bonus_amount*0.01=1ydb
		TaskOrder    int              `json:"task_order"`
	} `json:"task_attr"`
	TaskStat     TaskStat         `json:"task_stat"`
}

type TaskStat struct {
	CompleteCount          int `json:"complete_count"`
	TotalCompleteCount     int `json:"total_complete_count"`
	MultiDaysCompleteCount int `json:"multi_days_complete_count"`
	Status                 int `json:"status"`
}

type ApiCompleteTaskReq struct {
	Common dto.ApiReqCommon `json:"common"`
	TaskID string `json:"task_id"`
	RevenueType string `json:"revenue_type"`
}
type ApiCompleteTaskResp struct {
	Common dto.ApiRespCommon `json:"resp_common"`
	Exp          int `json:"exp"`
	YdbDelta     int `json:"ydb_delta"`
	RmbDelta     int `json:"rmb_delta"`
	VipYDBReward int `json:"vip_ydb_reward"`
}
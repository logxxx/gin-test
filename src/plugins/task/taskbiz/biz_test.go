package taskbiz_test

import (
	"context"
	"plugins/task/taskbiz"
	"plugins/testmain"
	"plugins/user/userdto"
	"testing"
	"time"
)

func TestTaskBiz_GetAvailableTaskIds(t *testing.T) {
	ctx := context.Background()
	now := time.Now()
	biz := taskbiz.GetInstance()
	req1 := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	resp1, err := biz.UserBiz.GetUsers(ctx, req1, now)
	if err != nil {
		t.Fatal(err)
	}
	if resp1 == nil {
		t.Fatal()
	}
	user := resp1.Users[1]
	resp2, err := biz.GetAvailableTaskIds(ctx, user.UidStr, user.Token, now)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("resp2:%v", resp2)

	for i := range resp2 {
		biz.CompleteTask(ctx, user.UidStr, user.Token, resp2[i], now)
	}
}

func TestTaskBiz_GetTasks(t *testing.T) {
	ctx := context.Background()
	now := time.Now()
	biz := taskbiz.GetInstance()
	req1 := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	resp1, err := biz.UserBiz.GetUsers(ctx, req1, now)
	if err != nil {
		t.Fatal(err)
	}
	if resp1 == nil {
		t.Fatal()
	}
	user := resp1.Users[1]
	resp2, err := biz.GetTasks(ctx, user.UidStr, user.Token, now)
	if err != nil {
		t.Fatal(err)
	}
	if resp2 == nil {
		t.Fatal()
	}
	for i := range resp2.Tasks {
		t.Logf("%#v", resp2.Tasks[i])
		resp3, err := biz.CompleteTask(ctx, user.UidStr, user.Token, resp2.Tasks[i].ID, now)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("resp:%#v", resp3)
		t.Log("\n")
		time.Sleep(1*time.Second)
	}
}

func TestMain(m *testing.M) {
	testmain.TestMain(m)
}
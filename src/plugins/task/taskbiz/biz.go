package taskbiz

import (
	"common/inject"
	"common/logutil"
	"context"
	"plugins/connection/connectionbiz/httpclient"
	"plugins/user/userbiz"
	"plugins/user/userdto"
	"time"
)

var (
	biz = &TaskBiz{}
	taskIdsCompleteByServer = []string{"2","3","4","5","16","9","6","110","135","134","14","31"} //只能由服务端完成的任务
)

type TaskBiz struct {
	UserBiz *userbiz.UserBiz `inject:""`
	HttpClient *httpclient.HttpClient `inject:"httpclient"`
}

func MustInit()  {
	inject.MustProvide(biz)
}

func GetInstance() *TaskBiz {
	return biz
}

func (biz *TaskBiz) InjectInit() error {
	logutil.Infof("TaskBiz.InjectInit()")
	now := time.Now()
	ctx := context.Background()
	getTokensReq := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	getTokensResp, err := biz.UserBiz.GetUsers(ctx, getTokensReq, now)
	if err != nil {
		logutil.Errorf("GetInfo_GetUsers_err:%v req:%v", err, getTokensReq)
		return err
	}

	for i := range getTokensResp.Users {
		uid := getTokensResp.Users[i].UidStr
		token := getTokensResp.Users[i].Token
		logutil.Infof("here go EventComplete")
		go biz.EventComplete(ctx, uid, token, now)
	}

	return nil
}
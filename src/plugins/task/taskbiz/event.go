package taskbiz

import (
	"common/logutil"
	"context"
	"time"
)

//事件:每个小时执行一次"完成所有任务"
func (biz *TaskBiz) EventComplete(ctx context.Context, uid, token string, now time.Time) error {
	for {
		taskIds, err := biz.GetAvailableTaskIds(ctx, uid, token, now)
		if err != nil {
			logutil.Errorf("err:%v", err)
			return err
		}

		for i := range taskIds {
			resp, err := biz.CompleteTask(ctx, uid, token, taskIds[i], now)
			if err != nil {
				logutil.Errorf("err:%v", err)
				return err
			}
			logutil.Infof("\tEventComplete uid:%v taskid:%v res:%#v", uid, taskIds[i], resp)
		}
		<- time.NewTimer(2 * time.Hour).C //每1小时执行一次
	}
	return nil
}
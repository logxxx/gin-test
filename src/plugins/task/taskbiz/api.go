package taskbiz

import (
	"common/dto"
	"common/logutil"
	"context"
	"plugins/task/taskdto"
	"time"
)

func (biz *TaskBiz) CompleteTask(ctx context.Context, uid, token string, taskId string, now time.Time) (*taskdto.ApiCompleteTaskResp, error) {
	time.Sleep(time.Millisecond*1500) //有个防刷锁
	path := dto.DomainCmwayx + "/cmway/task/complete"
	apiReq := &taskdto.ApiCompleteTaskReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiReq.TaskID = taskId
	apiResp := &taskdto.ApiCompleteTaskResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, apiResp)

	if err != nil {
		logutil.Errorf("CompleteTask_Post_err:%v req:%#v", err, apiReq)
		return nil, err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("fail.TaskID:%v resp.Common:%#v",
			taskId, apiResp.Common)
	} else {
		logutil.Infof("CompleteTask succ.TaskID:%v resp:%#v",
			taskId, apiResp)
	}
	return apiResp, nil
}

func (biz *TaskBiz) GetAvailableTaskIds(ctx context.Context, uid, token string, now time.Time) ([]string, error) {
	tasksResp, err := biz.GetTasks(ctx, uid, token, now)
	if err != nil {
		return nil, err
	}
	if len(tasksResp.Tasks) <= 0 {
		return nil, nil
	}

	respTaskIds := make([]string, 0)
	for i := range tasksResp.Tasks {
		if tasksResp.Tasks[i].TaskAttr.BonusAmount == 0 { continue }
		if tasksResp.Tasks[i].TaskStat.CompleteCount >= tasksResp.Tasks[i].TaskAttr.MaxExecCount { continue }
		isServerTask := false
		for j := range taskIdsCompleteByServer {
			if taskIdsCompleteByServer[j] == tasksResp.Tasks[i].ID {
				isServerTask = true
				break
			}
		}
		if isServerTask {continue}
		respTaskIds = append(respTaskIds, tasksResp.Tasks[i].ID)
	}

	logutil.Infof("GetAvailableTaskIds uid:%v taskids:%v", uid, respTaskIds)
	return respTaskIds, nil
}

func (biz *TaskBiz) GetTasks(ctx context.Context, uid, token string, now time.Time) (*taskdto.ApiGetTasksResp, error) {
	path := dto.DomainCmwayx + "/cmway/task/list"
	apiReq := dto.ApiReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiResp := taskdto.ApiGetTasksResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, &apiResp)
	if err != nil {
		logutil.Errorf("GetTasks_Post_err:%v req:%#v", err, apiReq)
		return nil, err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetTasks_Post_err:%v req:%#v resp.Common:%#v",
			"apiResp.Common.Ret != dto.ApiRespRetOK", apiReq, apiResp.Common)
		return nil, err
	}
	return &apiResp, nil
}

package task

import (
	"plugins/aixconfig"
	"plugins/task/taskbiz"
	"plugins/task/taskutil"
)

func MustInit() {
	cfg := &taskutil.Config{}
	aixconfig.MustLoadPluginConfig("task.json", cfg)

	taskbiz.MustInit()
}

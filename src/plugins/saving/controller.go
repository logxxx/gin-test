package saving

import (
	"common/logutil"
	"common/reqresputil"
	"github.com/gin-gonic/gin"
	"plugins/saving/savingbiz"
	"plugins/saving/savingdto"
	"time"
)

type GetAllInfoReq struct {
	reqresputil.Request
}

type GetAllInfoResp struct {
	reqresputil.Response
	Infos []savingdto.ApiGetInfoResp `json:"infos"`
}

func GetAllInfo(c *gin.Context) {
	req := &GetAllInfoReq{}
	resp := &GetAllInfoResp{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		logutil.Errorf("GetAllInfo_ParseBody_err:%v req:%#v", err, req)
		reqresputil.DoReturn(c, resp, err)
		return
	}

	biz := savingbiz.GetInstance()
	p := &savingdto.GetAllInfoParams{}
	dto, err := biz.GetAllInfo(c.Request.Context(), p, time.Now())
	if dto != nil {
		resp.Infos = dto.Infos
	}
	reqresputil.DoReturn(c, resp, err)
}

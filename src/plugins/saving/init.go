package saving

import (
	"plugins/aixrouter"
	"plugins/saving/savingbiz"
)

func MustInit() {
	savingbiz.MustInit()
	aixrouter.RegisterRouterFunc(InitRouter)
}
package saving


import "github.com/gin-gonic/gin"

func InitRouter(r gin.IRouter, prefix string) {
	r.POST(prefix+"/saving/get_all_info", GetAllInfo)
}
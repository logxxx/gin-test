package savingdto

import (
	"common/dto"
	"time"
)

type ApiGetInfoReq struct {
	Common dto.ApiReqCommon `json:"common"`
}

type ApiGetInfoResp struct {
	Common dto.ApiRespCommon `json:"resp_common"`
	PlayerInfo *PlayerInfo  `json:"player_info"`
	TaskInfo   *TaskConfig `json:"task_info"`
}

type ApiGetStateResp struct {
	Common dto.ApiRespCommon `json:"resp_common"`
	State struct {
		YdbToPoint          int `json:"ydb_to_point"`          //多少步换1ydb（配置）
		MinPointToExchange  int `json:"min_point_to_exchange"` //多少步起兑
		ExchangeablePoint   int `json:"exchangeable_point"`    //待兑换步数
		HarvestablePoint    int `json:"harvestable_point"`     //待收取步数
		MaxHarvestablePoint int `json:"max_harvestable_point"` //最大待收取步数（配置）
	} `json:"state"`
}

type PlayerInfo struct {
	Nickname        string `json:"nick_name" yaml:"nick_name"`
	AvatarUrl       string `json:"avatar_url" yaml:"avatar_url"`
	ScoreLimitFloor int64  `json:"score_limit_floor"`
	ScoreLimitTop   int64  `json:"score_limit_top"`
	Score           int64  `json:"score_delta"`
	Level           int    `json:"level_delta"`
	NextLevel       int    `json:"level_next"`
	Speed           int    `json:"speed"`
}

type TaskConfig struct {
	TaskInviteNew         TaskInviteNew     `json:"invite_new"`
	TaskExchangePoint     TaskExchangePoint `json:"exchange_point"`
	TaskMammonAssist      TaskMammonAssist  `json:"mammon_assist"`
	TaskPopularKing       TaskPopularKing   `json:"popular_king"`
	TaskTenfoldCardAssist TaskTenfoldCard  `json:"tenfold_card_assist"`
	TaskMeals             TaskMeals        `json:"meals,omitempty"`
}

type TaskInviteNew struct {
	Process int   `json:"process"`
	Bonus   int64 `json:"bonus"`
}

type TaskExchangePoint struct {
	Require int   `json:"require"`
	Process int   `json:"process"`
	Bonus   int64 `json:"bonus"`
}

type TaskMammonAssist struct {
	Bonus             int64 `json:"bonus"`
	CanAssist         bool  `json:"can_assist"`
	CooldownSec       int   `json:"cooldown_sec"`
	RemainCooldownSec int   `json:"remain_cooldown_sec"`
}

type TaskPopularKing struct {
	Require int   `json:"require"`
	Process int   `json:"process"`
	Bonus   int64 `json:"bonus"`
}

type TaskTenfoldCard struct {
	BonusScore        int64 `json:"bonus"`               //奖励分数
	Status            int   `json:"status"`              //状态(非配置)
	CooldownSec       int   `json:"cooldown_sec"`        //冷却时间(秒)
	RemainCooldownSec int   `json:"remain_cooldown_sec"` //剩余冷却时间(非配置)
	Multiple          int   `json:"multiple_delta"`      //翻倍倍数
}

type TaskMeals struct {
	Bonus           int64          `json:"bonus"`             //奖励(已废弃)
	MealNow         int            `json:"meal_now"`          //当前处于第x餐(x为meal_id)
	Meals           []Meal         `json:"meals_info"`        //每餐信息配置
	BonusPointRange [2]int         `json:"bonus_point_range"` //奖励步数范围
	BonusPoint      int            `json:"bonus_point"`       //奖励步数值(非配置)
	BonusScore      int64          `json:"bonus_score"`       //奖励分数
	Status          int            `json:"status"`            //1:未到时间 2:可以领取 3:已领取
	Toast           TaskMealsToast `json:"toast"`             //弹窗提示
	EndTime         int64          `json:"end_time"`          //当前餐结束时间
	Desc            string         `json:"desc"`              //描述
}

type TaskMealsToast struct {
	IsToday  bool `json:"is_today"`  //toash提示"今天/明天"
	NextHour int  `json:"next_hour"` //toast提示"xx点来领取"
}

type Meal struct {
	Id        int       `json:"id"`
	Start     string    `json:"start"`
	End       string    `json:"end"`
	StartTime time.Time `json:"-"`
	EndTime   time.Time `json:"-"`
	Desc      string    `json:"desc"`
}

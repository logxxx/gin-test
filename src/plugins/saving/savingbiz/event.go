package savingbiz

import (
	"common/logutil"
	"context"
	"math/rand"
	"plugins/user/userdto"
	"time"
)

func getMidnightSec(now time.Time) int {
	tomrrow := now.AddDate(0,0,1)
	tomrrow = time.Date(tomrrow.Year(), tomrrow.Month(), tomrrow.Day(), 0, 0, 0, 0, tomrrow.Location())
	return int(tomrrow.Sub(now).Seconds())
}


//获取系统赠送的机器人
//每天首次登陆获得，所以每天0左右点执行一次
func (biz *SavingBiz) EventGetRobot(uid, token string) {
	for {
		logutil.Infof(">>> EventGetRobot start.uid:%v", uid)
		now := time.Now()
		nextSec := getMidnightSec(now)
		rand.Seed(now.Unix())
		offset := rand.Intn(600)
		t := time.NewTimer(time.Duration(nextSec+offset)*time.Second)//12:00~12:10之间会执行一次
		<- t.C
		biz.DoAssist(context.Background(), uid, token, AssistTypeRobot)
		logutil.Infof("<<< EventGetRobot end.uid:%v", uid)
	}
}

//获取财神
//系统启动立即执行一次，
//然后根据cd，Sleep一段时间。
//之后再次执行
func (biz *SavingBiz) EventGetMammonOrTenFoldCard(uid, token string, assistType int) {
	ctx := context.Background()
	for {
		logutil.Infof(">>> EventGetMammonOrTenFoldCard(%v) start.uid:%v", assistType, uid)
		//先无脑执行
		biz.DoAssist(context.Background(), uid, token, assistType)
		//然后查cd时间
		inforesp, err := biz.GetInfo(ctx, uid, token)
		if err != nil {
			logutil.Errorf("EventGetMammon GetInfo err:%v", err)
			return
		}
		cd := inforesp.TaskInfo.TaskMammonAssist.RemainCooldownSec
		if cd < 10 {
			cd = 10 //防止DOS
		}
		logutil.Infof("\t> EventGetMammonOrTenFoldCard.GetInfo cd:%v 当前时间:%v 下次执行时间:%v", cd, time.Now(), time.Now().Add(time.Duration(cd)*time.Second))
		<- time.NewTimer(time.Duration(cd) * time.Second).C
	}
	return
}

func (biz *SavingBiz) EventDoHarvest(uid, token string) {
	ctx := context.Background()
	for {
		logutil.Infof(">>> EventDoHarvest() start.uid:%v", uid)
		_, err := biz.DoHarvest(ctx, uid, token)
		if err != nil {
			logutil.Errorf("EventDoHarvest DoHarvest err:%v", err)
			return
		}
		//先查有多少步数
		resp, err := biz.GetState(ctx, uid, token)
		if err != nil {
			logutil.Errorf("err:%v", err)
			return
		}
		logutil.Infof("\t EventDoHarvest min:%v exchangeable:%v", resp.State.MinPointToExchange, resp.State.ExchangeablePoint)
		if resp.State.ExchangeablePoint > resp.State.MinPointToExchange {
			logutil.Infof("do exchange.")
			err = biz.DoExchange(ctx, uid, token)
			if err != nil {
				logutil.Errorf("err:%v", err)
				return
			}
		}
		<- time.NewTimer(10 * time.Minute).C //每30分钟收割一次
	}
}

func (biz *SavingBiz) EventAssistEachOther() error {
	ctx := context.Background()
	getTokensReq := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	getTokensResp, err := biz.UserBiz.GetUsers(context.Background(), getTokensReq, time.Now())
	if err != nil {
		logutil.Errorf("GetInfo_GetUsers_err:%v req:%v", err, getTokensReq)
		return err
	}
	for {
		for i := range getTokensResp.Users {
			for j := range getTokensResp.Users {
				uidA := getTokensResp.Users[i].UidStr
				tokenA := getTokensResp.Users[i].Token
				uidB := getTokensResp.Users[j].UidStr
				tokenB := getTokensResp.Users[j].Token
				if uidA != uidB {
					err := biz.AAssistB(ctx, uidA, tokenA, uidB, tokenB)
					if err != nil {
						logutil.Errorf("err:%v", err)
						return err
					}
				}
			}
		}
		//助力规则
		//1.一天只能助力5次
		//2.一天只能给同一个好友助力一次
		//所以这个程序一天跑一次就行了
		now := time.Now()
		nextSec := getMidnightSec(now)
		rand.Seed(now.Unix())
		offset := rand.Intn(600)
		t := time.NewTimer(time.Duration(nextSec+offset)*time.Second)//12:00~12:10之间会执行一次
		<-t.C
	}
	return nil
}
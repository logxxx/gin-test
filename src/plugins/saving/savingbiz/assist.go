package savingbiz

import (
	"common/dto"
	"common/logutil"
	"context"
)

var (
	AssistTypeRobot = 0
	AssistTypeMammon = 2
	AssistTypeTenfoldCard = 3
)

func (biz *SavingBiz) DoAssist(ctx context.Context, uid string, token string, assistType int) error {
	path := dto.DomainCmwayx + "/cmway/saving/robot_assists"
	type Req struct {
		Common dto.ApiReqCommon `json:"common"`
		RequireType int `json:"type"`
	}
	type Resp struct {
		Common dto.ApiRespCommon `json:"resp_common"`
	}
	req := Req{}
	req.Common.Token = token
	req.Common.Uid = uid
	req.RequireType = assistType
	resp := Resp{}
	err := biz.HttpClient.Post(ctx, path, &req, &resp)
	if err != nil {
		logutil.Errorf("GetUsers_Post_err:%v req:%#v", err, req)
		return err
	}
	return nil
}
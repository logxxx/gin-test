package savingbiz

import (
	"common/code"
	"common/dto"
	"common/inject"
	"common/logutil"
	"context"
	"plugins/connection/connectionbiz/httpclient"
	"plugins/saving/savingdto"
	"plugins/user/userbiz"
	"plugins/user/userdto"
	"time"
)

var (
	biz = &SavingBiz{}
)

type SavingBiz struct {
	UserBiz *userbiz.UserBiz `inject:""`
	HttpClient *httpclient.HttpClient `inject:"httpclient"`
}

func MustInit()  {
	inject.MustProvide(biz)
}

func (biz *SavingBiz) InjectInit() error {
	getTokensReq := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	getTokensResp, err := biz.UserBiz.GetUsers(context.Background(), getTokensReq, time.Now())
	if err != nil {
		logutil.Errorf("GetInfo_GetUsers_err:%v req:%v", err, getTokensReq)
		return err
	}

	for i := range getTokensResp.Users{
		uid := getTokensResp.Users[i].UidStr
		token := getTokensResp.Users[i].Token
		go biz.EventGetRobot(uid, token)
		go biz.EventGetMammonOrTenFoldCard(uid, token, AssistTypeMammon)
		go biz.EventGetMammonOrTenFoldCard(uid, token, AssistTypeTenfoldCard)
		go biz.EventDoHarvest(uid, token)
	}

	go biz.EventAssistEachOther()

	return nil
}

func GetInstance() *SavingBiz {
	return biz
}

func (biz *SavingBiz) AAssistB(ctx context.Context, uidA, tokenA, uidB, tokenB string) error {
	//思路:A调用share_report,带上b的uid
	path := dto.DomainCmwayx + "/cmway/share/report"
	type ShareReportReq struct {
		Common dto.ApiReqCommon `json:"common"`
		ParentId string `json:"parent_id"`
		ShareType string `json:"share_type"`
	}
	type ShareReportResp struct {
		Common dto.ApiRespCommon 				`json:"resp_common"`
	}

	req := ShareReportReq{}
	req.Common.Uid = uidA
	req.Common.Token = tokenA
	req.ParentId = uidB
	req.ShareType = "8"
	resp := ShareReportResp{}
	err := biz.HttpClient.Post(ctx, path, req, &resp)
	if err != nil {
		logutil.Errorf("AAssistB_Post_err:%v req:%#v", err, req)
		return err
	}
	if resp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("AAssistB_Post_err:%v req:%#v resp.Common:%#v",
			"resp.Common.Ret != dto.ApiRespRetOK", req, resp.Common)
		return code.ServerError
	}

	return nil
}

func (biz *SavingBiz) AssistAll(ctx context.Context, p *savingdto.AssistAllParams, now time.Time) (*savingdto.AssistAllDto, error) {
	getTokensReq := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	getTokensResp, err := biz.UserBiz.GetUsers(ctx, getTokensReq, now)
	if err != nil {
		logutil.Errorf("GetInfo_GetUsers_err:%v req:%v", err, getTokensReq)
	}
	for i := range getTokensResp.Users {
		err := biz.DoAssist(ctx, getTokensResp.Users[i].UidStr, getTokensResp.Users[i].Token, p.AssistType)
		if err != nil {
			logutil.Errorf("AssistAll_Post_err:%v", err)
			return nil, err
		}
	}
	return nil, nil
}

func(biz *SavingBiz) DoExchange(ctx context.Context, uid, token string) (error) {
	path := dto.DomainCmwayx + "/cmway/saving/exchange_point"
	type ExchangeReq struct {
		Common dto.ApiReqCommon `json:"common"`
	}
	type ExchangeResp struct {
		Common dto.ApiRespCommon 				`json:"resp_common"`
		ExchangedPoint int   `json:"exchanged_point"`
		YdbDelta       int   `json:"ydb_delta"`
		YdbBalance     int64 `json:"ydb_balance"`
	}
	req := ExchangeReq{}
	req.Common.Uid = uid
	req.Common.Token = token
	resp := ExchangeResp{}
	err := biz.HttpClient.Post(ctx, path, req, &resp)
	if err != nil {
		logutil.Errorf("DoHarvest_Post_err:%v req:%#v", err, req)
		return err
	}
	if resp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetInfo_Post_err:%v req:%#v resp.Common:%#v",
			"resp.Common.Ret != dto.ApiRespRetOK", req, resp.Common)
		return err
	}
	logutil.Infof("DoExchange %v->%v ydbBalance:%v", resp.ExchangedPoint, resp.YdbDelta, resp.YdbBalance)
	return nil
}

func(biz *SavingBiz) DoHarvest(ctx context.Context, uid, token string) (int, error){
	path := dto.DomainCmwayx + "/cmway/saving/harvest_point"
	type DoHarvestReq struct {
		Common dto.ApiReqCommon `json:"common"`
	}
	type DoHarvestResp struct {
		Common dto.ApiRespCommon 				`json:"resp_common"`
		HarvestedPoint int                      `json:"harvested_point"`
	}
	req := DoHarvestReq{}
	req.Common.Uid = uid
	req.Common.Token = token
	resp := DoHarvestResp{}
	err := biz.HttpClient.Post(ctx, path, req, &resp)
	if err != nil {
		logutil.Errorf("DoHarvest_Post_err:%v req:%#v", err, req)
		return 0, err
	}
	if resp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetInfo_Post_err:%v req:%#v resp.Common:%#v",
			"resp.Common.Ret != dto.ApiRespRetOK", req, resp.Common)
		return 0, err
	}
	return resp.HarvestedPoint, nil
}

func (biz *SavingBiz) GetState(ctx context.Context, uid, token string) (*savingdto.ApiGetStateResp, error) {
	path := dto.DomainCmwayx + "/cmway/saving/get_state"
	apiReq := savingdto.ApiGetInfoReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiResp := &savingdto.ApiGetStateResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, apiResp)
	if err != nil {
		logutil.Errorf("GetState_Post_err:%v req:%#v", err, apiReq)
		return nil, err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetState_Post_err:%v req:%#v resp.Common:%#v",
			"apiResp.Common.Ret != dto.ApiRespRetOK", apiReq, apiResp.Common)
		return nil, err
	}
	return apiResp, nil
}

func (biz *SavingBiz) GetInfo(ctx context.Context, uid, token string) (*savingdto.ApiGetInfoResp, error) {
	path := dto.DomainCmwayx + "/cmway/saving/get_info"
	apiReq := savingdto.ApiGetInfoReq{}
	apiReq.Common.Uid = uid
	apiReq.Common.Token = token
	apiResp := savingdto.ApiGetInfoResp{}
	err := biz.HttpClient.Post(ctx, path, apiReq, &apiResp)
	if err != nil {
		logutil.Errorf("GetInfo_Post_err:%v req:%#v", err, apiReq)
		return nil, err
	}
	if apiResp.Common.Ret != dto.ApiRespRetOK {
		logutil.Errorf("GetInfo_Post_err:%v req:%#v resp.Common:%#v",
			"apiResp.Common.Ret != dto.ApiRespRetOK", apiReq, apiResp.Common)
		return nil, err
	}
	return &apiResp, nil
}

func (biz *SavingBiz) GetAllInfo(ctx context.Context, p *savingdto.GetAllInfoParams, now time.Time) (*savingdto.GetAllInfoDto, error) {
	getTokensReq := &userdto.GetUsersParams{
		JustNeedToken: true,
	}
	getTokensResp, err := biz.UserBiz.GetUsers(ctx, getTokensReq, now)
	if err != nil {
		logutil.Errorf("GetInfo_GetUsers_err:%v req:%v", err, getTokensReq)
		return nil, err
	}

	respInfos := make([]savingdto.ApiGetInfoResp, 0)
	for i := range getTokensResp.Users {
		tmp := getTokensResp.Users[i]
		apiResp, err := biz.GetInfo(ctx, tmp.UidStr, tmp.Token)
		if err != nil {
			logutil.Errorf("GetAllInfo_GetInfo_err:%v", err)
			return nil, err
		}
		respInfos = append(respInfos, *apiResp)
	}

	return &savingdto.GetAllInfoDto{
		Infos: respInfos,
	}, nil
}
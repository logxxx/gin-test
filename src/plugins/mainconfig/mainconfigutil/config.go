package mainconfigutil

import "plugins/aixconfig"

var gConfig = &Config{}

type Config struct {
	Log LogConfig `json:"log"`
	Inject InjectConfig `json:"inject"`
}

type InjectConfig struct {
	Route map[string]string `json:"route"`
}

type LogConfig struct {
	Level string `json:"level"`
	LogDir string `json:"log_dir"`
}

func MustInit() {
	aixconfig.MustLoadPluginConfig("main.json", gConfig)
}

func GetConfig() Config {
	return *gConfig
}

package mainconfig

import "plugins/mainconfig/mainconfigutil"

func MustInit() {
	mainconfigutil.MustInit()
}
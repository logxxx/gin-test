package statistic

import (
	"common/logutil"
	"common/randutil"
	"common/reqresputil"
	"github.com/gin-gonic/gin"
	"plugins/statistic/statisticbiz"
	"plugins/statistic/statisticdto"
	"time"
)

type GetStatisticsReq struct {
	reqresputil.Request
}

type GetStatisticsResp struct {
	reqresputil.Response
	Statistics []StatisticInfo `json:"statistics"`
}

type StatisticInfo struct {
	ProjectName string `json:"project_name"`
	MergeByMe int `json:"merge_by_me"`
	MergeByOther int `json:"merge_by_other"`
}

func GetStatistics(c *gin.Context) {
	resp := &GetStatisticsResp{}

	projectNameList := []string{"农场赚qq", "农场赚android", "运动赚ios", "运动赚qq", "运动赚ios", "运动赚android"}
	for i:=0; i<6; i++ {
		resp.Statistics = append(resp.Statistics, StatisticInfo{
			ProjectName:projectNameList[i],
			MergeByMe:randutil.NewRand().Intn(20),
			MergeByOther:randutil.NewRand().Intn(100),
		})
	}

	reqresputil.DoReturn(c, resp, nil)
}

type GetStatisticsByDateReq struct {
	reqresputil.Request
}

type GetStatisticsByDateResp struct {
	reqresputil.Response
	StatisticsByDate []StatisticByDateInfo `json:"statistics"`
}

type StatisticByDateInfo struct {
	Date string `json:"date"`
	MergeByMeTotal int `json:"merge_by_me_total"`
	MergeByOtherTotal int `json:"merge_by_other_total"`
}

func GetStatisticsByDate(c *gin.Context) {
	resp := &GetStatisticsByDateResp{}

	now := time.Now()
	for i := 0; i < 7; i++ {
		newTime := now.AddDate(0, 0, -i)
		resp.StatisticsByDate = append(resp.StatisticsByDate, StatisticByDateInfo{
			Date: newTime.Format("2006-01-02"),
			MergeByMeTotal:randutil.NewRand().Intn(50),
			MergeByOtherTotal:randutil.NewRand().Intn(200),
		})
	}

	reqresputil.DoReturn(c, resp, nil)
}

type GetCountReq struct {
	reqresputil.Request
}

type GetCountResp struct {
	reqresputil.Response
	Today int `json:"today"`
	Week int `json:"week"`
	Total int `json:"total"`
}

func GetCount(c *gin.Context) {
	resp := &GetCountResp{
		Today:randutil.NewRand().Intn(100),
		Week:randutil.NewRand().Intn(1000),
		Total:randutil.NewRand().Intn(10000),
	}

	reqresputil.DoReturn(c, resp, nil)
}

type DoLikeReq struct {
	reqresputil.Request
}

type DoLikeResp struct {
	reqresputil.Response
	Count int `json:"count"`
}

func DoLike(c *gin.Context) {
	req := &DoLikeReq{}
	resp := &DoLikeResp{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		logutil.Errorf("DoLike_ParseBody_err:%v req:%#v", err, req)
		reqresputil.DoReturn(c, resp, err)
		return
	}

	biz := statisticbiz.GetInstance()

	p := &statisticdto.DoLikeParams{}
	dto, err := biz.DoLike(reqresputil.GetContext(c), p, time.Now())
	if dto != nil {
		resp.Count = dto.Count
	}
	reqresputil.DoReturn(c, resp, err)
}

type GetLikeCountReq struct {
	reqresputil.Request
}

type GetLikeCountResp struct {
	reqresputil.Response
	Count int `json:"count"`
}

func GetLikeCount(c *gin.Context) {
	req := &GetLikeCountReq{}
	resp := &GetLikeCountResp{}
	err := reqresputil.ParseBody(c, req)
	if err != nil {
		logutil.Errorf("GetLikeCount_ParseBody_err:%v req:%#v", err, req)
		reqresputil.DoReturn(c, resp, err)
		return
	}

	biz := statisticbiz.GetInstance()

	p := &statisticdto.GetLikeCountParams{}
	dto, err := biz.GetLikeCount(reqresputil.GetContext(c), p, time.Now())
	if dto != nil {
		resp.Count = dto.Count
	}
	reqresputil.DoReturn(c, resp, err)
}
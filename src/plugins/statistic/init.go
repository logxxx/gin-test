package statistic

import (
	"plugins/aixrouter"
	"plugins/statistic/statisticbiz"
	"plugins/statistic/statisticdao"
)

func MustInit(){

	statisticdao.MustInit()
	statisticbiz.MustInit()

	aixrouter.RegisterRouterFunc(InitRouter)
}
package statisticbiz

import (
	"common/inject"
	"common/logutil"
	"context"
	"plugins/statistic/statisticdao"
	"plugins/statistic/statisticdto"
	"time"
)

var (
	biz = &StatisticBiz{}
)


type StatisticBiz struct {
	Dao *statisticdao.StatisticDao `inject:""`
}

func (biz *StatisticBiz) Close() error {
	return nil
}

func GetInstance() *StatisticBiz {
	return biz
}

func MustInit() {
	inject.MustProvide(biz)
}


func (biz *StatisticBiz) DoLike(ctx context.Context, params *statisticdto.DoLikeParams, now time.Time) (*statisticdto.DoLikeDto, error) {
	res, err := biz.Dao.IncrDoLikeCount(1, now)
	if err != nil {
		logutil.Infof("DoLkie err:%v", err)
		return nil, err
	}
	return &statisticdto.DoLikeDto{
		Count: res,
	}, nil
}

func (biz *StatisticBiz) GetLikeCount(ctx context.Context, params *statisticdto.GetLikeCountParams, now time.Time) (*statisticdto.GetLikeCountDto, error) {
	res, err := biz.Dao.GetLikeCount(now)
	if err != nil {
		logutil.Infof("GetLikeCount err:%v", err)
		return nil, err
	}
	return &statisticdto.GetLikeCountDto{
		Count: res,
	}, nil
}
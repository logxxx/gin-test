package statistic

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func InitRouter(r gin.IRouter, prefix string) {
	r.GET(prefix+"/statistic/get_projects_analyse", GetStatistics)
	r.GET(prefix+"/statistic/get_projects_analyse_bydate", GetStatisticsByDate)
	r.GET(prefix+"/statistic/get_count", GetCount)
	r.GET(prefix+"/statistic/do_like", DoLike)
	r.GET(prefix+"/statistic/get_like_count", GetLikeCount)
	r.StaticFS(prefix+"/statistic/frontend", http.Dir("./frontend")) //前端页面渲染
}
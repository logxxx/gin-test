package statisticdao

import (
	"fmt"
	"gopkg.in/redis.v5"
	"time"
)

var (
	prefix = "statistic"
)

func keyDoLike() string {
	return fmt.Sprintf("%v:dolike", prefix)
}

func (dao *StatisticDao) IncrDoLikeCount(delta int, now time.Time) (int, error) {
	key := keyDoLike()
	res, err := dao.Redis.IncrBy(key, int64(delta)).Result()
	if err != nil {
		return 0, err
	}
	return int(res), nil
}

func (dao *StatisticDao) GetLikeCount(now time.Time) (int, error) {
	key := keyDoLike()
	res, err := dao.Redis.Get(key).Int64()
	if err != nil {
		if err == redis.Nil {
			return 0, nil
		}
		return 0, err
	}
	return int(res), nil
}
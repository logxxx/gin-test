package statisticdao

import (
	"common/inject"
	"gopkg.in/redis.v5"
)

var (
	dao = &StatisticDao{}
)

type StatisticDao struct {
	Redis *redis.Client `inject:"@redis-main"`
}

func (dao *StatisticDao) Close() error {
	return dao.Redis.Close()
}

func MustInit() {
	inject.MustProvide(dao)
}
package dto

var (
	DomainCmwayx = "http://ydz-app.cmcm.com"
	ApiRespRetOK = 0
)

type ApiReqCommon struct {
	Uid string `json:"uid"`
	Token string `json:"token"`
}

type ApiRespCommon struct {
	Ret int `json:"ret"`
	Msg string `json:"msg"`
}

type ApiReq struct {
	Common ApiReqCommon `json:"common"`
}

type ApiResp struct {
	Common ApiRespCommon `json:"resp_common"`
}
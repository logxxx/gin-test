package resource

import (
	"errors"
	"fmt"
	"runtime"
	"sync"
)

var (
	ErrObjectHasBeenInit = errors.New("object has been initialized")

	pool = &closerPool{
		objects: make(map[string]Closer),
		lock:    new(sync.RWMutex),
	}
)

type closerPool struct {
	objects map[string]Closer
	keys    []string
	lock    *sync.RWMutex
}

type Closer interface {
	Close() error
}

func Set(name string, value Closer) error {
	pool.lock.Lock()
	defer pool.lock.Unlock()
	if _, ok := pool.objects[name]; ok {
		return ErrObjectHasBeenInit
	}

	pool.objects[name] = value
	pool.keys = append(pool.keys, name)
	return nil
}

func Get(name string) Closer {
	pool.lock.RLock()
	defer pool.lock.RUnlock()
	return pool.objects[name]
}

func Shutdown() error {
	pool.lock.Lock()
	defer pool.lock.Unlock()

	for i := len(pool.keys) - 1; i >= 0; i-- {
		k := pool.keys[i]
		o := pool.objects[k]

		err := func() (err error) {
			defer func() {
				if e := recover(); e != nil {
					err = fmt.Errorf("panic recover while closing `%s`: %v", k, e)
				}
			}()
			err = o.Close()
			return
		}()
		if err != nil {
			return err
		}
	}
	pool.objects = nil
	pool.keys = nil
	return nil
}

func mustGetPackageName(skip int) string {
	pc, _, _, ok := runtime.Caller(skip)
	if !ok {
		panic("fail to get resource name")
	}
	details := runtime.FuncForPC(pc)
	return details.Name()
}

func MustGetPackageNameWithSuffix(suffix string) string {
	return mustGetPackageName(2) + "." + suffix
}

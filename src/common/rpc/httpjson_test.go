package rpc

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"datax-common/log"
)

type reqResp struct {
	SleepMs    int    `json:"sleep_ms"`
	StatusCode int    `json:"status_code"`
	DoPanic    string `json:"do_panic"`
}

type typeP map[string]interface{}

func createTestServer() {

	go func() {
		mux := &http.ServeMux{}
		mux.HandleFunc("/debug", func(w http.ResponseWriter, r *http.Request) {
			req := &reqResp{}

			defer r.Body.Close()
			data, _ := ioutil.ReadAll(r.Body)
			err := json.Unmarshal(data, req)

			if err != nil {
				w.WriteHeader(400)
				return
			}

			if req.SleepMs > 0 {
				time.Sleep(time.Millisecond * time.Duration(req.SleepMs))
			}
			if req.DoPanic != "" {
				panic(req.DoPanic)
			}
			w.Write(data)
		})

		mux.HandleFunc("/bad_resp", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(`{...}`))
		})

		mux.HandleFunc("/code_500", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(500)
			w.Write([]byte(`{}`))
		})

		s := http.Server{
			Addr:    "127.0.0.1:15567",
			Handler: mux,
		}
		if s.ListenAndServe() != nil {
			panic("listen")
		}
	}()

}

func TestJRpc(t *testing.T) {
	log.SetLogLevel(log.DEBUG)

	createTestServer()

	c := NewJRpc(JRpcConfig{
		Service:   "hello",
		URL:       "http://localhost:15567",
		TimeoutMS: 20, // 全局超时
	})
	defer c.Close()

	// 正常
	out := &reqResp{}
	err := c.Invoke(nil, "/debug", &reqResp{SleepMs: 10}, out)
	if err != nil {
		t.Fatalf("err: %#v", err)
	}
	if out.SleepMs != 10 || out.DoPanic != "" {
		t.Fatalf("unexpected result: %#v", out)
	}

	// ctx超时
	out = &reqResp{}
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*10) // ctx超时
	defer cancel()
	err = c.Invoke(ctx, "/debug", &reqResp{SleepMs: 15}, out)
	if e := ConvertToRpcError(err); e == nil || e.Code != 0 || e.Type != ErrTypeTimeout {
		t.Fatalf("expect err: %s", err)
	}

	// 全局超时
	out = &reqResp{}
	err = c.Invoke(nil, "/debug", &reqResp{SleepMs: 21}, out)
	if e := ConvertToRpcError(err); e == nil || e.Code != 0 || e.Type != ErrTypeTimeout {
		realErr, _ := err.(*url.Error)
		t.Fatalf("expect err: %#v", realErr)
	}

	// 异常（panic）
	out = &reqResp{}
	err = c.Invoke(nil, "/debug", &reqResp{SleepMs: 10, DoPanic: "mypanic"}, out)
	if e := ConvertToRpcError(err); e == nil || e.Code != 0 || e.Type != ErrRpcError {
		t.Fatalf("expect err: %s", err)
	}

	// 异常（500）
	out = &reqResp{}
	err = c.Invoke(nil, "/code_500", &reqResp{SleepMs: 10, DoPanic: "mypanic"}, out)
	if e := ConvertToRpcError(err); e == nil || e.Code != 500 || e.Type != ErrTypeBadResponse {
		t.Fatalf("expect err: %s", err)
	}

	// 异常（404）
	out = &reqResp{}
	err = c.Invoke(nil, "/no_exists", &reqResp{SleepMs: 10, DoPanic: "mypanic"}, out)
	if e := ConvertToRpcError(err); e == nil || e.Code != 404 || e.Type != ErrTypeBadResponse {
		t.Fatalf("expect err: %s", err)
	}

	// 不存在
	c2 := NewJRpc(JRpcConfig{
		Service:   "hello",
		URL:       "http://localhost:2000",
		TimeoutMS: 2000,
	})
	defer c2.Close()

	err = c2.Invoke(nil, "/no_exists", &reqResp{SleepMs: 10, DoPanic: "mypanic"}, &reqResp{})
	if e := ConvertToRpcError(err); e == nil || e.Code != 0 || e.Type != ErrRpcError {
		t.Fatalf("expect err: %s", err)
	}
}

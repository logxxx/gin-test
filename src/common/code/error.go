package code

import "fmt"

const (
	clientErr = 4
	serverErr = 5
	logicErr  = 6
)

var _ error = BaseError{}

type BaseError struct {
	Ret int
	Msg string
	err error
}

func (e BaseError) Error() string {
	if e.err != nil {
		return fmt.Sprintf("(%d)%s, err='(%T)%s'", e.Ret, e.Msg, e.err, e.err)
	}
	return fmt.Sprintf("(%d)%s", e.Ret, e.Msg)
}

func newErrorWithCode(ret int, msg string) BaseError {
	return BaseError{
		Ret: ret,
		Msg: msg,
	}
}

func NewError(ret int, msg string) BaseError {
	return newErrorWithCode(ret, msg)
}

func (e BaseError) WithErr(err error) BaseError {
	b := e
	b.err = err
	b.Msg = err.Error()
	return b
}

func (e BaseError) Equals(e2 BaseError) bool {
	return e.Ret == e2.Ret
}

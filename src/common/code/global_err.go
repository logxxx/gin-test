package code

var (
	OK = newErrorWithCode(0, "ok")

	BadRequest  = newErrorWithCode(4001001, "bad request")
	ServerError = newErrorWithCode(5001001, "server error")
)

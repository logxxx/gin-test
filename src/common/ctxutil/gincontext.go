package ctxutil

import (
	"context"
	"fmt"
)

const (
	ctxRequestInfo = "ctx.request.info.__"
)

// F 打印出带上下文的日志
func F(ctx context.Context, format string, args ...interface{}) string {
	req := GetReqInfo(ctx)
	prefix := fmt.Sprintf("[m=%s rid=%s ip=%s uid=%d]",
		req.MetricName, req.RequestId, req.ClientIp, req.Uid)

	msg := format
	if len(args) > 0 {
		msg = fmt.Sprintf(msg, args...)
	}
	return prefix + msg
}

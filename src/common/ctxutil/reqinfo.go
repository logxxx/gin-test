package ctxutil

import (
	"common/randutil"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"sync"
	"time"
)

type ReqInfo struct {
	ClientIp string
	RequestId string
	Uid string

	StartTime time.Time
	EndTime time.Time

	MetricName string
	ReqData []byte
	HasReadBody bool
	ReqObject interface{}
	RespData []byte
	RespObject interface{}
	RequestFinished bool

	mvalues sync.Map
}

func (r *ReqInfo) Set(key, value interface{}) {
	r.mvalues.Store(key, value)
}

func (r *ReqInfo) Get(key interface{}) interface{} {
	if v, ok := r.mvalues.Load(key); ok {
		return v;
	}
	return nil
}

func GetReqInfo(ctx context.Context) *ReqInfo {
	if v := ctx.Value(ctxRequestInfo); v != nil {
		if reqInfo, ok := v.(*ReqInfo); ok {
			return reqInfo
		}
	}
	return &ReqInfo{}
}

func SetReqInfo(c *gin.Context) *ReqInfo {
	r := &ReqInfo{}
	c.Set(ctxRequestInfo, r)
	return r
}

func (r *ReqInfo) SetReqData(reqData []byte) {
	r.ReqData = reqData
	r.HasReadBody = true
}

func (r *ReqInfo) SetRespData(respData []byte) {
	r.RespData = respData
	r.RequestFinished = true
}

func (r *ReqInfo) SetRespObject(obj interface{}) {
	r.RespObject = obj
	r.RequestFinished = true
}

func InitContext(c *gin.Context) {
	reqStartTime := time.Now()
	s := randutil.NewRand().Int31()
	rid := fmt.Sprintf("%x-%x", reqStartTime.Unix(), s)
	reqInfo := SetReqInfo(c)
	reqInfo.RequestId = rid
	reqInfo.ClientIp = c.ClientIP()
	reqInfo.StartTime = reqStartTime
	reqInfo.MetricName = c.Request.URL.Path

	return
}

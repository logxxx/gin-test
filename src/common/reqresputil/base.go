package reqresputil

import (
	"common/logutil"
	"strconv"
)

type IRequest interface {
	GetRequestCommon() *RequestCommon
}

type IResponse interface {
	GetResponseCommon() *ResponseCommon
}

type Request struct {
	Common RequestCommon `json:"common"`
}

type RequestCommon struct {
	Uid string `json:"uid"`
	RequestId string `json:"request_id"`
}

type Response struct {
	Common ResponseCommon `json:"common"`
}

type ResponseCommon struct {
	Ret       int    `json:"ret"`
	Message   string `json:"message"`
	RequestId string `json:"request_id"`
}

func (r *Request) GetRequestCommon() *RequestCommon {
	return &r.Common
}

func (r *Response) GetResponseCommon() *ResponseCommon {
	return &r.Common
}

func (r *Request) GetUid() (int64, error) {
	uid, err := strconv.ParseInt(r.Common.Uid, 10, 64)
	if err != nil {
		logutil.Errorf("parse uid: %s error: %v", r.Common.Uid, err)
		return 0, err
	}
	return uid, nil
}
package reqresputil

import (
	"common/code"
	"common/ctxutil"
	"common/logutil"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
)

func ParseBody(c *gin.Context, req interface{}) error {
	reqInfo := ctxutil.GetReqInfo(c)
	bodyData := reqInfo.ReqData
	if !reqInfo.HasReadBody {
		var err error
		bodyData, err = c.GetRawData()
		if err != nil {
			logutil.Errorf("reqresputil.ParseBody err:%v len:%v", err, len(bodyData))
		}
		if len(bodyData) == 0 {
			return nil
		}
		reqInfo.SetReqData(bodyData)
	}

	err := json.Unmarshal(bodyData, req)
	if err != nil {
		logutil.Errorf("ParseBody-json.Unmarshal-err:%v param:%#v", err, req)
		return err
	}
	reqInfo.ReqObject = req

	//获取common
	if iReq, ok := req.(IRequest); ok {
		common := iReq.GetRequestCommon()
		if common.Uid != "" {
			reqInfo.Uid = common.Uid
		}
		if common.RequestId != "" {
			reqInfo.RequestId = common.RequestId
		}
	}

	return nil
}

func returnJson(c *gin.Context, resp interface{}) {
	//设置common
	if iReq, ok := resp.(IResponse); ok {
		common := iReq.GetResponseCommon()
		if common.Message == "" {
			common.Message = "ok"
		}
		if common.RequestId == "" {
			common.RequestId = ctxutil.GetReqInfo(c).RequestId
		}
	}

	ctxutil.GetReqInfo(c).SetRespObject(resp)
	data, err := json.Marshal(resp)
	if err != nil {
		logutil.Errorf("ReturnJson-json.Marshal-err:%v param:%v", err, resp)
		ginResponseData(c, []byte(err.Error()))
	}

	ginResponseData(c, data)
}

func ginResponseData(c *gin.Context, data[]byte) {
	ctxutil.GetReqInfo(c).SetRespData(data)
	c.Writer.Write(data)
	c.Abort()
}

func returnBaseError(c *gin.Context, error code.BaseError) {
	resp := &Response{}
	resp.Common.Ret = error.Ret
	resp.Common.Message = error.Msg
	returnJson(c, resp)
	return
}

func DoReturn(c *gin.Context, resp IResponse, err error) {
	if err == nil {
		returnJson(c, resp)
		return
	}

	bErr, ok := err.(code.BaseError)
	if !ok {
		baseErr := code.ServerError.WithErr(err)
		returnBaseError(c, baseErr)
		return
	}

	if !bErr.Equals(code.OK) {
		returnBaseError(c, bErr)
		return
	}

	returnJson(c, resp)
	return
}

func Stringify(obj interface{}) string {
	if obj == nil {
		return "null"
	}

	data, err := json.Marshal(obj)
	if err != nil {
		logutil.Errorf("reqresputil.Stringify err:%v obj:%v", err, obj)
		return fmt.Sprintf("reqresputil.Stringify err:%#v", err)
	}
	return string(data)
}

func GetContext(c *gin.Context) context.Context {
	return c.Request.Context()
}
package randutil

import (
	crand "crypto/rand"
	"encoding/binary"
	"math/rand"
	"sync/atomic"
	"time"
)

var seed int64 = 0

func init() {
	s0 := time.Now().UnixNano()
	buf := make([]byte, 8)
	if _, err := crand.Read(buf); err == nil {
		s0 ^= int64(binary.BigEndian.Uint64(buf))
	}
	atomic.StoreInt64(&seed, s0)
}

func NewRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano()))
}

func RandInt(min int64, max int64) int64 {
	if min >= max || min == 0 || max == 0 {
		return max
	}
	return rand.Int63n(max-min) + min
}

package logutil

import (
	"fmt"
	"log"
	"sync/atomic"
)

const (
	NOTSET = 1 << iota

	DEBUG

	INFO

	WARN

	ERROR

	FATAL
)

var levelMapping = map[string]int{
	"NOTSET": NOTSET,
	"DEBUG": DEBUG,
	"INFO": INFO,
	"WARN": WARN,
	"ERROR": ERROR,
	"FATAL": FATAL,
}

var defaultLogLevel int64 = INFO

func InitFlags() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func SetLogLevel(level int) {
	atomic.StoreInt64(&defaultLogLevel, int64(level))
}

func SetStringLogLevel(level string) bool {
	v, ok := levelMapping[level]
	if !ok {
		return ok
	}
	SetLogLevel(v)
	return ok
}

func EnableLogFor(level int) (bool) {
	l := atomic.LoadInt64(&defaultLogLevel)
	return int64(level) >= l
}

func Infof(fmt string, args ...interface{}) {
	if EnableLogFor(DEBUG) {
		msg := sprintf("[INFO]" + fmt, args...)
		log.Output(2, msg)
	}
}

func Debugf(fmt string, args ...interface{}) {
	if EnableLogFor(DEBUG) {
		msg := sprintf("[DEBUG]" + fmt, args...)
		log.Output(2, msg)
	}
}

func Errorf(fmt string, args ...interface{}) {
	if EnableLogFor(DEBUG) {
		msg := sprintf("[ERROR]" + fmt, args...)
		log.Output(2, msg)
	}
}

func Fatal(msg string, err error) {
	m := sprintf("[FATAL]%s: %s", msg, err)
	log.Output(2, m)
	panic(err)
}

func Output(calldepth int, fmt string, args ...interface{}) {
	msg := sprintf(fmt, args...)
	log.Output(calldepth+1, msg)
}

func sprintf(format string, args ...interface{}) string {
	if len(args) <= 0 {
		return format
	}
	return fmt.Sprintf(format, args...)
}
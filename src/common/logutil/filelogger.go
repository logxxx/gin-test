package logutil

import (
	"fmt"
	"os"
	"path"
	"sync"
	"time"
)

const (
	fileFormatPrefix = "2006-01-02"
	logTimeLayout = "060102 15:04:05"
	onDaySeconds = 24 * 60 * 60
)

type Logger struct {
	unixSeconds int64
	pid int
	stdout bool
	dir string
	file *os.File
	lock sync.Mutex
}

func NewLogger(dir string) (log *Logger, err error) {
	log = &Logger{
		pid: os.Getpid(),
		dir: dir,
		lock: sync.Mutex{},
	}

	if dir == "" {
		log.stdout = true
	} else {
		log.stdout = false
	}
	if !log.stdout {
		var err error
		if _, err = os.Stat(dir); err != nil {
			if os.IsNotExist(err) {
				err = os.MkdirAll(dir, 0755)
			}
		}
		if err != nil {
			return nil, err
		}

		t := time.Now()
		err = log.newLogFile(t)
		if err != nil {
			return nil, err
		}
		log.unixSeconds = truncateTime(t).Unix()
	}
	return log, nil
}

func (log *Logger) newLogFile(t time.Time) (err error) {
	fileName := fmt.Sprintf("server.%s.log",t.Format(fileFormatPrefix))
	fileName = path.Join(log.dir, fileName)

	file, err := os.OpenFile(fileName, os.O_WRONLY | os.O_CREATE | os.O_APPEND, 0664)
	if err != nil {
		return err
	}
	log.file.Sync()
	log.file.Close()
	log.file = file

	return nil
}

func truncateTime(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func (log *Logger) Close() {
	if !log.stdout {
		log.file.Sync()
		log.file.Close()
	}
}
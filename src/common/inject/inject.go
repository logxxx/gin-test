package inject

import (
	"errors"
	"fmt"
	"io"
	"reflect"
	"runtime"
	"strings"
	"sync"

	"common/logutil"
)

// Inspired by https://github.com/facebookgo/inject

//revive:disable:exported

const (
	injectTagKey   = "inject"
	routeTagPrefix = "@" // 开启名字路由之后，包含这个前缀的对象都会根据配置来选择对应的注入对象
)

var (
	defaultGraph = NewGraph(defaultLogger(1))

	ErrInvalidProviderType         = errors.New("invalid provider type")
	ErrDuplicatedProvider          = errors.New("duplicated provider")
	ErrProviderNotFound            = errors.New("provider not found")
	ErrMultipleProvidersToSetField = errors.New("multiple providers to set field")
)

//revive:enable:exported

type defaultLogger int

// R Doc: 添加注释
type R map[string]string

// InjectIniter 的实现会在被注入后（调用 MustInject后）被执行
type InjectIniter interface {
	InjectInit() error
}

// Debugf Doc: 添加注释
func (l defaultLogger) Debugf(format string, args ...interface{}) {
	logutil.Infof(format, args...)
}

// Object Doc: 添加注释
type Object struct {
	// Name optional
	Name  string
	Value interface{}

	//revive:disable:var-naming
	type_  reflect.Type
	value_ reflect.Value
	//revive:enable:var-naming

	dependencies          []string
	numFields             int
	numFieldsToInject     int
	numFieldsToInjectLeft int
	location              string
}

// IsCompleted Doc: 添加注释
func (o *Object) IsCompleted() bool {
	return o.numFieldsToInjectLeft == 0
}

// DependenciesLack Doc: 添加注释
func (o *Object) DependenciesLack() string {
	obj := o
	deps := make([]string, 0, 1)
	for ix := 0; ix < obj.numFields; ix++ {
		field := obj.type_.Elem().Field(ix)
		if !fieldNeedToInject(field) {
			continue
		}

		valueField := obj.value_.Elem().FieldByName(field.Name)
		if !valueField.IsNil() {
			continue
		}
		deps = append(deps, valueField.String())
	}
	return strings.Join(deps, ",")
}

// String Doc: 添加注释
func (o *Object) String() string {
	return fmt.Sprintf("o(%q,t=%q.%q,ok=%v)",
		o.location, o.Name, o.type_.String(), o.IsCompleted())
}

// DebugLogger Doc: 添加注释
type DebugLogger interface {
	Debugf(string, ...interface{})
}

// Graph Doc: 添加注释
type Graph struct {
	Logger DebugLogger

	objectsByNameMap map[string]*Object
	objectsList      []*Object
	objectsCompleted []*Object

	routeConfig R

	locker *sync.RWMutex
}

// NewGraph Doc: 添加注释
func NewGraph(logger DebugLogger) *Graph {
	return &Graph{
		Logger:           logger,
		locker:           new(sync.RWMutex),
		objectsByNameMap: make(map[string]*Object),
	}
}

// Debugf Doc: 添加注释
func (g *Graph) Debugf(format string, args ...interface{}) {
	if g.Logger != nil {
		g.Logger.Debugf(format, args...)
	}
}

func numFields(v interface{}) int {
	t := reflect.TypeOf(v)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() == reflect.Struct {
		return t.NumField()
	}
	return 0
}

// MustProvideByName Doc: 添加注释
func (g *Graph) MustProvideByName(name string, v interface{}) {
	g.mustProvideByName(name, v, 3)
}
func (g *Graph) mustProvideByName(name string, v interface{}, skip int) {
	t := reflect.TypeOf(v)
	numFieldsToInject := len(getAllFieldsWithInjectTag(t))
	obj := &Object{
		Name:                  name,
		Value:                 v,
		type_:                 t,
		value_:                reflect.ValueOf(v),
		dependencies:          nil,
		numFields:             numFields(v),
		numFieldsToInject:     numFieldsToInject,
		numFieldsToInjectLeft: numFieldsToInject,
		location:              mustGetInitLocation(skip),
	}

	if !g.IsValidProvider(v) {
		g.Debugf("not a valid provider: %s", obj)
		panic(ErrInvalidProviderType)
	}

	g.locker.Lock()
	defer g.locker.Unlock()
	if name != "" {
		if oldObject, ok := g.objectsByNameMap[name]; ok {
			g.Debugf("object with same name(=%s) is register before: old=%s new=%s",
				oldObject, obj)
			panic(ErrDuplicatedProvider)
		}
		g.objectsByNameMap[name] = obj
	}
	if obj.IsCompleted() {
		g.objectsCompleted = append(g.objectsCompleted, obj)
	}

	g.objectsList = append(g.objectsList, obj)
	g.Debugf("provided: %s", obj)
}

// MustProvide Doc: 添加注释
func (g *Graph) MustProvide(v interface{}) {
	g.mustProvideByName("", v, 3)
}

// isValidStrutType 判断对象类型是否（非指针）struct类型并且有需要注入的对象
func (g *Graph) isValidStrutType(t reflect.Type) bool {
	if t.Kind() == reflect.Struct {
		for i := 0; i < t.NumField(); i++ {
			f := t.Field(i)
			if fieldNeedToInject(f) {
				g.Debugf("provider %q 由于是非pointer类型，不能包含注入对象 %q", t, displayField(f))
				return false
			}
		}
	}
	return true
}

func displayField(f reflect.StructField) string {
	return fmt.Sprintf("%s %s %s", f.Name, f.Type.String(), f.Tag)
}

// IsValidProvider 检查对象是否合法的provider
// 合法的 Provider 支持类型如下：
// 		*struct{} 类型对象
//			- 不包含 inject 对象
//			- 包含 inject 对象，并且
// 				对象类型都是是 interface{} 或者指针类型，并且
//				如果成员变量是 interface{} 类型的话，tag必须有值，并且
//				这些成员变量值都是 nil
//		struct{} 类型对象
//			成员变量不能包含有 inject 的 tag
//		其他类型的对象
//			都支持
func (g *Graph) IsValidProvider(v interface{}) bool {
	t := reflect.TypeOf(v)

	if !g.isValidStrutType(t) {
		return false
	}

	if !isStructPtr(t) {
		return true
	}

	for _, f := range getAllFieldsWithInjectTag(t) {
		if !g.isValidInjectedFieldType(t, f) {
			g.Debugf("provider %q 包含一个无效的注入成员: %q", t, displayField(f))
			return false
		}

		// 需要注入的对象只能是nil值
		if !reflect.ValueOf(v).Elem().FieldByName(f.Name).IsNil() {
			return false
		}
	}
	return true
}

func (g *Graph) isValidInjectedFieldType(structT reflect.Type, f reflect.StructField) bool {
	vType := f.Type
	if vType.Kind() == reflect.Interface {
		// interface{} 类型的注入字段必须包含名字
		if f.Tag.Get(injectTagKey) == "" {
			g.Debugf("字段 %q . %q 是interface{}类型，但是tag为空", structT, displayField(f))
			return false
		}
		return true
	}
	if len(f.Name) > 0 {
		firstChar := f.Name[0]
		if firstChar < 'A' || firstChar > 'Z' {
			g.Debugf("字段 %q . %q 不是public（大写字母开头）", structT, displayField(f))
			return false
		}
	}
	if vType.Kind() != reflect.Ptr {
		g.Debugf("字段 %q . %q 不是指针类型", structT, displayField(f))
		return false
	}
	return true
}

func fieldNeedToInject(f reflect.StructField) bool {
	_, ok := f.Tag.Lookup(injectTagKey)
	return ok
}

func getAllFieldsWithInjectTag(t reflect.Type) (o []reflect.StructField) {
	if !isStructPtr(t) {
		return
	}
	t = t.Elem()
	for ix := 0; ix < t.NumField(); ix++ {
		f := t.Field(ix)
		if fieldNeedToInject(f) {
			o = append(o, f)
		}
	}

	return
}

func mustSetField(f reflect.Value, v reflect.Value) {
	if !f.CanSet() {
		// never here
		logutil.Errorf("can not be set(must be ptr type and upper case): %s(%s)",
			f.String(), f.Type().String())
		panic("value can not set")
	}
	f.Set(v)
}

func (g *Graph) setInjectField(field reflect.StructField, fieldValue reflect.Value) (*Object, error) {
	tagName := field.Tag.Get(injectTagKey)
	targetObjs := make([]*Object, 0, 1)

	if tagName != "" {
		oldTagName := tagName
		// 指定名字
		if g.isRouteEnable() {
			// 路由
			if g.isRouteKey(tagName) {
				tagName = g.getRouteTarget(tagName)
			}
		} else {
			// 路由关闭，但是key包含前缀，基于防御性策略，直接报错
			if g.isRouteKey(tagName) {
				panic(fmt.Errorf("invalid inject key=%q (forgot to enable route?)", tagName))
			}
		}
		if targetObj, ok := g.objectsByNameMap[tagName]; ok {
			if targetObj.IsCompleted() {
				targetObjs = append(targetObjs, targetObj)
			}
		} else {
			// 提前失败
			panic(fmt.Errorf("not found object: name=%q", oldTagName))
		}
	} else {
		// 非指定名字，按类型匹配
		for _, targetObj := range g.objectsCompleted {
			if targetObj.type_ == fieldValue.Type() {
				targetObjs = append(targetObjs, targetObj)
			}
		}
	}

	if len(targetObjs) == 0 {
		return nil, ErrProviderNotFound
	}
	if len(targetObjs) > 1 {
		return nil, ErrMultipleProvidersToSetField
	}

	target := targetObjs[0]
	mustSetField(fieldValue, target.value_)
	return target, nil
}

func (g *Graph) setObject(obj *Object) (completed bool, err error) {

	for ix := 0; ix < obj.numFields; ix++ {
		field := obj.type_.Elem().Field(ix)
		if !fieldNeedToInject(field) {
			continue
		}

		valueField := obj.value_.Elem().FieldByName(field.Name)
		if !valueField.IsNil() {
			continue
		}

		target, err := g.setInjectField(field, valueField)
		if target != nil {
			// 设置字段成功
			obj.numFieldsToInjectLeft--
			g.Debugf("assigned: %s to %s.%s", target, obj, field.Name)
		}

		if err == ErrProviderNotFound {
			// 有可能只是依赖的对象暂时还没初始化
			err = nil
		}
		if err != nil {
			return false, err
		}
	}
	if obj.IsCompleted() {
		g.objectsCompleted = append(g.objectsCompleted, obj)
		g.Debugf("completed: %s", obj)
		return true, nil
	}
	return false, nil
}

// InspectResult Doc: 添加注释
func (g *Graph) InspectResult() string {
	g.locker.Lock()
	defer g.locker.Unlock()

	res := []string{""}
	for _, obj := range g.objectsList {
		if obj.IsCompleted() {
			continue
		}
		lack := obj.DependenciesLack()
		res = append(res, fmt.Sprintf("====> Object(%s) lacks: %s", obj.String(), lack))
	}
	return strings.Join(res, "\n\t")
}

// MustInject Doc: 添加注释
func (g *Graph) MustInject() {
	g.locker.Lock()
	defer g.locker.Unlock()

MainLoop:
	for {
		doSomething := false
		for _, obj := range g.objectsList {
			if obj.IsCompleted() {
				continue
			}

			isSet, err := g.setObject(obj)
			if err != nil {
				g.Debugf("fail to set Object(%s): %s", obj, err)
				panic(err)
			}
			if isSet {
				doSomething = true
			}
		}

		if !doSomething {
			// 如果循环内没有依赖被注入，并且循环结束后还有对象没有注入完成
			// 则表示依赖不满足
			for _, obj := range g.objectsList {
				if !obj.IsCompleted() {
					err := fmt.Errorf("broken dependencies for object: %s, lack deps: %s",
						obj, obj.DependenciesLack())
					panic(err)
				}
			}
			break MainLoop
		}
	}

	for ix := range g.objectsCompleted {
		obj := g.objectsCompleted[ix]
		initer, ok := obj.Value.(InjectIniter)
		if !ok {
			continue
		}
		err := initer.InjectInit()
		if err != nil {
			g.Debugf("fail to init object(%s): %v", obj, err)
			panic(err)
		}
		g.Debugf("inited: object=%s", obj)
	}
}

func isStructPtr(t reflect.Type) bool {
	return t.Kind() == reflect.Ptr && t.Elem().Kind() == reflect.Struct
}

func mustGetInitLocation(skip int) string {
	pc, _, line, ok := runtime.Caller(skip)
	if !ok {
		panic("fail to get resource name")
	}
	fn := runtime.FuncForPC(pc)
	return fmt.Sprintf("%s:%d", fn.Name(), line)
}

// Close Doc: 添加注释
func (g *Graph) Close() error {
	g.locker.Lock()
	defer g.locker.Unlock()
	for ix := len(g.objectsCompleted) - 1; ix >= 0; ix-- {
		obj := g.objectsCompleted[ix]
		if closer, ok := obj.Value.(io.Closer); ok {
			err := func() (err error) {
				defer func() {
					if pnc := recover(); pnc != nil {
						err = fmt.Errorf("<PANIC(%s)>", pnc)
					}
				}()
				err = closer.Close()
				return
			}()
			if err != nil {
				g.Debugf("closed: object=%s err=%v", obj, err)
			}
		}
	}
	return nil
}

// Get Doc: 添加注释
func (g *Graph) Get(name string) interface{} {
	g.locker.Lock()
	defer g.locker.Unlock()
	if obj, ok := g.objectsByNameMap[name]; ok {
		return obj.Value
	}

	return nil
}

func (g *Graph) isRouteKey(key string) bool {
	return strings.HasPrefix(key, routeTagPrefix)
}

func (g *Graph) isRouteEnable() bool {
	return g.routeConfig != nil
}

func (g *Graph) getRouteTarget(name string) string {
	return g.routeConfig[name]
}

// EnableRoute Doc: 添加注释
func (g *Graph) EnableRoute(route R) {
	newRoute := make(R)
	for k, v := range route {
		if !g.isRouteKey(k) {
			panic(fmt.Errorf("invalid route key=%q", k))
		}
		if v == "" {
			panic(fmt.Errorf("empty route value with key=%q", k))
		}
		newRoute[k] = v
	}
	g.locker.Lock()
	defer g.locker.Unlock()
	if g.routeConfig != nil {
		panic(fmt.Errorf("EnableRoute call twice: old=%v new=%v", g.routeConfig, newRoute))
	}
	g.routeConfig = newRoute
}

// MustProvide 声明不带名字的Provider
func MustProvide(obj interface{}) {
	defaultGraph.mustProvideByName("", obj, 3)
}

// MustProvideByName 声明一个带名字的（名字不能冲突）Provider
// Provider 会在 MustInject() 调用后，根据名字或者类型注入其他Provider的成员变量中。
// Provider可以是任何类型。
// 如果Provider包含需要注入的成员变量，需要同时满足以下条件：
//		* Provider 只能是 *struct{} 类型；
//		* 需要被注入的成员变量（带上"inject"的标签）都是指针/接口类型，并且是nil值；
func MustProvideByName(name string, obj interface{}) {
	defaultGraph.mustProvideByName(name, obj, 3)
}

// MustInject 注入对象到Provider中
func MustInject() {
	defer func() {
		msg := defaultGraph.InspectResult()
		if msg != "" {
			defaultGraph.Debugf("Inject fail: %s", msg)
		}
	}()
	defaultGraph.MustInject()
}

// Get 根据名字获取对象
func Get(name string) interface{} {
	return defaultGraph.Get(name)
}

// EnableRoute 开启注入名字映射，以 '@' 开头的注入名字会根据配置被映射。
// 函数需要再 MustInject 前辈调用。
// 函数不能被多次调用。
func EnableRoute(routeConfig R) {
	defaultGraph.EnableRoute(routeConfig)
}

// MustGet Doc: 添加注释
func MustGet(name string) interface{} {
	obj := defaultGraph.Get(name)
	if obj == nil {
		panic("provider not found")
	}
	return obj
}

// Close 如果 Provider 实现了 io.Closer 接口，会按初始化逆序调用 Close() 接口。
func Close() error {
	return defaultGraph.Close()
}

// DisableLogging Doc: 添加注释
func DisableLogging() {
	defaultGraph.Logger = nil
}

// SetLogging Doc: 添加注释
func SetLogging(lg DebugLogger) {
	defaultGraph.Logger = lg
}

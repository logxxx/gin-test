package middleware

import (
	"common/ctxutil"
	"common/logutil"
	"common/reqresputil"
	"github.com/gin-gonic/gin"
	"time"
)

func SetContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctxutil.InitContext(c)
		c.Next()
	}
}

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next() //先执行完了函数本体，才打印的log。而req是在Parse()阶段产生的

		reqInfo := ctxutil.GetReqInfo(c)
		req := reqresputil.Stringify(reqInfo.ReqObject)
		resp := reqresputil.Stringify(reqInfo.RespObject)

		endTime := time.Now()
		reqInfo.EndTime = endTime
		ct := secondsDelta(reqInfo.StartTime, reqInfo.EndTime)

		logutil.Infof("ct=%.2f rid=%v url=%v req=%q resp=%q",
			ct,
			reqInfo.RequestId,
			reqInfo.MetricName,
			trimBodyDataString(req, 1024),
			trimBodyDataString(resp, 1024),
		)
	}
}

func milliSecondsDelta(s, e time.Time) float64 {
	return float64(e.Sub(s)/time.Microsecond)
}

func secondsDelta(s, e time.Time) float64 {
	return float64(e.Sub(s)/time.Second)
}

func trimBodyDataString(s string, maxSize int) string {
	if maxSize <= 0 {
		return s
	}
	if len(s) > maxSize {
		return s[:maxSize] + "..."
	}
	return s
}
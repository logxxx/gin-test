package test

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func Test(t *testing.T) {
	fmt.Println("hehe")

	u := &MyUser{
		Id :1,
		Name: "hehe",
		LastSeen: time.Now(),
	}

	res, err := u.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("%v\n", string(res))
}

type MyUser struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	LastSeen time.Time `json:"last_seen"`
}

func (u *MyUser) MarshalJSON1() ([]byte, error) {
	return json.Marshal(&struct {
		Id int64 `json:"id"`
		Name string `json:"name"`
		LastSeen int64 `json:"last_seen"`
	}{
		Id:u.Id,
		Name: u.Name,
		LastSeen: u.LastSeen.Unix(),
	})
}

func (u *MyUser) MarshalJSON2() ([]byte, error) {
	return json.Marshal(&struct {
		LastSeen int64 `json:"last_seen"`
		*MyUser
	}{
		LastSeen: u.LastSeen.Unix(),
		MyUser: u,
	})
}

func (u *MyUser) MarshalJSON() ([]byte, error) {
	type Alias MyUser
	return json.Marshal(&struct {
		LastSeen int64 `json:"last_seen"`
		*Alias
	}{
		LastSeen: u.LastSeen.Unix(),
		Alias: (*Alias)(u),
	})
}
FROM golang:latest

RUN mkdir /mytest
WORKDIR /mytest
COPY ./ ./
WORKDIR /mytest
ENV GOPATH /mytest
RUN go build /mytest/src/cmd/main.go

EXPOSE 8080
ENTRYPOINT ["./main"]